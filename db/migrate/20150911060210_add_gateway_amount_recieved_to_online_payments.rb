class AddGatewayAmountRecievedToOnlinePayments < ActiveRecord::Migration
  def change
    add_column :online_payments, :gateway_amount_recieved, :decimal
  end
end
