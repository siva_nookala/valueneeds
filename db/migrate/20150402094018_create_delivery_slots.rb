class CreateDeliverySlots < ActiveRecord::Migration
  def change
    create_table :delivery_slots do |t|
      t.datetime :start_date_time
      t.datetime :end_date_time
      t.boolean :enable
      
      t.timestamps
    end
  end
end
