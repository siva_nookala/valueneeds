class ProductsController < ApplicationController
  include ApplicationHelper::SessionConstants
  include ProductsHelper
  include ApplicationHelper
  include PromosHelper
  include CategoriesHelper
  filter_resource_access
  filter_access_to :order_online, :attribute_check => false
  filter_access_to :add_items_to_cart, :attribute_check => false
  filter_access_to :remove_order_item, :attribute_check => false
  filter_access_to :show_cart, :attribute_check => false
  filter_access_to :login_screen, :attribute_check => false
  filter_access_to :remove_order, :attribute_check => false
  filter_access_to :add_child_form, :attribute_check => false
  filter_access_to :update_category_child, :attribute_check => false
  filter_access_to :remove_category, :attribute_check => false
  filter_access_to :update_product_location_prices, :attribute_check => false
  filter_access_to :add_product_location_price_form, :attribute_check => false
  filter_access_to :remove_location_price, :attribute_check => false
  filter_access_to :update_location, :attribute_check => false
  filter_access_to :empty_cart, :attribute_check => false
  filter_access_to :get_products, :attribute_check => false
  filter_access_to :get_products_by_ids, :attribute_check => false
  filter_access_to :load_auto_complete_search_products, :attribute_check => false
  filter_access_to :clear_auto_complete_text, :attribute_check => false
  filter_access_to :get_main_filtered_products, :attribute_check => false
  filter_access_to :show_search_products, :attribute_check => false
  filter_access_to :product_details_tab, :attribute_check => false
  filter_access_to :product_request, :attribute_check => false
  filter_access_to :request_products, :attribute_check => false
  filter_access_to :clean_orders, :attribute_check => false
  filter_access_to :change_product_selection_quantity, :attribute_check => false
  filter_access_to :change_product_tab_selection_quantity, :attribute_check => false
  filter_access_to :edit_product, :attribute_check => false
  filter_access_to :add_items_to_wish_list, :attribute_check => false
  filter_access_to :show_wish_list_products, :attribute_check => false
  filter_access_to :remove_wish_list_items, :attribute_check => false
  filter_access_to :load_more, :attribute_check => false
  # GET /products
  # GET /products.json
  def index
    limit = !params[:limit].blank? ? params[:limit] : 100
    offset = !params[:offset].blank? ? params[:offset] : 0
    categories = []
    if !params[:category_id].blank?
      CategoriesHelper.admin_get_categories_by_parent(params[:category_id], categories)
      categories = categories.collect { |category| category.id.to_i }
      categories << params[:category_id].to_i
    end
    @products = Product.search_scope(params[:search_option]).category_scope(categories).brand_scope(params[:brand_id]).price_scope(params[:price_range_id])
    @all_products_size = @products.count
    @products = @products.limit(limit).offset(offset)
    respond_to do |format|
      format.html # index.html.erb
      format.js
      @products.each do |product|
        product.picture = root_url+'product/'+product.picture if !product.picture.blank?
      end
      format.json { render json: @products }
    end
  end

  # GET /products/1
  # GET /products/1.json
  def show
    @product = Product.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @product }
    end
  end

  # GET /products/new
  # GET /products/new.json
  def new
    @product = Product.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @product }
    end
  end

  # GET /products/1/edit
  def edit
    @product = Product.find(params[:id])
    @child_count = 0
    @location_price_child_count = 0
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new
    update_product
    respond_to do |format|
      if @product.save
        format.html { redirect_to edit_product_path(@product), notice: 'Product was successfully created.' }
        format.json { render json: @product, status: :created, location: @product }
      else
        format.html { render action: "new" }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /products/1
  # PUT /products/1.json
  def update
    @product = Product.find(params[:id])
    update_product
    respond_to do |format|
      if @product.save
        format.html { redirect_to edit_product_path(@product), notice: 'Product was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product = Product.find(params[:id])
    @product.destroy

    respond_to do |format|
      format.html { redirect_to products_url }
      format.json { head :ok }
    end
  end

  def update_product
    @product.name = params[:product][:name]
    @product.brand_id = params[:product][:brand_id]
    @product.price = params[:product][:price]
    @product.active = params[:product][:active]
    @product.description=params[:product][:description]
    @product.html_title=params[:product][:html_title]
    @product.html_keywords=params[:product][:html_keywords]
    @product.html_description=params[:product][:html_description]
    @product.is_attribute = params[:product][:is_attribute]
    @product.is_hot_deal = params[:product][:is_hot_deal]
    @product.out_of_stock = params[:product][:out_of_stock]
    @product.needs_payment = params[:product][:needs_payment]
    @product.pre_order = params[:product][:pre_order]
    if params[:product][:discount_type] != 'No-Discount'
      @product.discount_type = params[:product][:discount_type]
      @product.discount_value = params[:product][:discount_value]
    end
    product_price_range = ProductPriceRange.where('start_price<=? and end_price>=?', @product.price, @product.price).first
    @product.product_price_range_id = product_price_range.id if !product_price_range.blank?
    @product.quantity = params[:product][:quantity]
    @product.group_id = params[:product][:group_id]

    set_picture_to_product(:picture, 'product')
    set_picture_to_product(:large_picture, 'product/l')

  end

  def set_picture_to_product(picture_variable, directory)
    uploaded_io = params[:product][picture_variable]
    if uploaded_io
      file_path_name = UUIDTools::UUID.random_create.to_s.delete '-'
      just_filename = File.basename(uploaded_io.original_filename)
      file_path_name += just_filename.sub(/[^\w\.\-]/, '_')
      File.open(Rails.root.join('public', directory, file_path_name), 'w+b') do |file|
        file.write(uploaded_io.read)
      end
      @product.update_attribute(picture_variable, file_path_name)
    end


  end

  def add_child_form
    @child_count = params[:child_count].to_i+1
  end

  def update_category_child
    params.keys.each do |key|
      if (key.start_with?('category_'))
        index = (key.split('_')[1])
        product_category = ProductCategory.new
        product_category.product_id = params[:product_id]
        product_category.category_id = params["category_#{index}"]
        product_category.save!
      end
    end
    redirect_to "/products/#{params[:product_id]}/edit"
  end

  def remove_category
    @product_category = ProductCategory.find_by_id(params[:product_category_id])
    @product_category.destroy
  end

  def add_product_location_price_form
    @location_price_child_count = params[:location_price_child_count].to_i+1
  end

  def update_product_location_prices
    params.keys.each do |key|
      if (key.start_with?('location_price_'))
        product_price = ProductPrice.new
        index = (key.split('_')[2])
        product_price.product_id = params[:product_id]
        product_price.price = params["location_price_#{index}"]
        product_price.location_id = params["location_#{index}"]
        if params["discount_type_#{index}"] != 'No-Discount'
          product_price.discount_type = params["discount_type_#{index}"]
          product_price.discount_value = params["discount_value_#{index}"]
          puts params["discount_type_#{index}"]
          puts product_price.discount_type
          puts product_price.discount_value
        end
        product_price.save!
      end
    end
    redirect_to "/products/#{params[:product_id]}/edit"
  end

  def remove_location_price
    @product_price = ProductPrice.find_by_id(params[:product_price_id].to_i)
    @product_price.destroy
  end

  def order_online

  end

  def add_items_to_cart
    @product = Product.find(params[:id])
    unless @product.nil?
      session[SESSION_ID] = UUIDTools::UUID.random_create.to_s.delete '-' if session[SESSION_ID].blank?
      matched_item = !current_user.blank? ? CartItem.find_by_user_id_and_item_id(current_user.id, @product.id) : CartItem.find_by_session_id_and_item_id(session[SESSION_ID], @product.id)
      if matched_item.blank?
        cart_order_item = CartItem.new
        cart_order_item.user_id = current_user.id if !current_user.blank?
        cart_order_item.session_id = session[SESSION_ID]
        cart_order_item.item_id = @product.id
        cart_order_item.quantity = params[:quantity]
        location_id = !session[BUYER_LOCATION].blank? ? session[BUYER_LOCATION] : Location.first.id
        product_location_price = ProductPrice.find_by_location_id_and_product_id(location_id, @product.id)
        effective_price = !product_location_price.blank? ? product_location_price.price : @product.price
        cart_order_item.price = effective_price
        product_attribute = ''
        attribute_index = 0
        params.keys.each do |key|
          if key.start_with?('attribute_name_')
            product_attribute += params["attribute_name_#{attribute_index}"]
            product_attribute += ' , ' if params["attribute_size"].to_i > attribute_index+1
            attribute_index = attribute_index+1
          end
        end
        cart_order_item.product_attributes = product_attribute if !product_attribute.blank?
        cart_order_item.save
      else
        matched_item.quantity = matched_item.quantity.to_i+1
        matched_item.save
      end
    end
    respond_to do |format|
      format.js
      format.json { render json: {status: true} }
    end
  end

  def remove_order_item
    @product = Product.find(params[:id])
    if !@product.blank?
      matched_item = !current_user.blank? ? CartItem.find_by_user_id_and_item_id(current_user.id, @product.id) : CartItem.find_by_session_id_and_item_id(session[SESSION_ID], @product.id)
      if !matched_item.blank?
        if matched_item.quantity.to_i-1==0
          matched_item.destroy
        else
          matched_item.quantity = matched_item.quantity.to_i-1
          matched_item.save
        end
      end
      respond_to do |format|
        format.js
        if request.format=='json'
          format.json { render json: {:status => !matched_item.blank?} }
        end
      end
    end
  end

  def show_cart
    if !current_user.blank?
      @cart_items = CartItem.find_all_by_user_id(current_user.id)
    else
      @cart_items = CartItem.find_all_by_session_id(session[SESSION_ID])
    end
    respond_to do |format|
      format.html
      format.json { render json: get_cart_line_items(@cart_items) }
    end
  end


  def login_screen


  end

  def remove_order
    @product = Product.find_by_id(params[:id])
    if !@product.blank?
      @cart_item = !current_user.blank? ? CartItem.find_by_user_id_and_item_id(current_user.id, @product.id) : CartItem.find_by_session_id_and_item_id(session[SESSION_ID], @product.id)
      if !@cart_item.blank?
        @cart_item.destroy
      end
    end
    respond_to do |format|
      if request.format=='json'
        format.json { render json: {:status => true} }
      else
        format.html { redirect_to '/products/show_cart' }
      end
    end

  end

  def update_location
    if !params[:location].blank?
      session[ApplicationHelper::SessionConstants::BUYER_LOCATION] = params[:location]
      session[SESSION_ORDER_ITEMS]=[]
    end
    redirect_to root_url
  end

  def empty_cart
    @cart_items = !current_user.blank? ? CartItem.find_all_by_user_id(current_user.id) : CartItem.find_all_by_session_id(session[SESSION_ID])
    @cart_items.each do |cart_item|
      cart_item.destroy
    end
    respond_to do |format|
      if request.format=='json'
        format.json { render json: {:status => true} }
      else
        format.html { redirect_to '/products/show_cart' }
      end
    end
  end

  def get_products
    get_all_filtered_products
    respond_to do |format|
      format.js
      format.json { render json: {:product_ids => @products.collect { |product| product.id }.uniq, :brand_ids => @brands.collect { |brand| brand.id }, :price_range_ids => @price_range_ids, :all_products_size => @all_products_size} }
    end
  end

  def get_main_filtered_products
    get_all_filtered_products
  end

  def get_products_by_ids
    @products = Product.active.find(:all, :conditions => ['id in (?)', params[:product_ids].split(',')])
    respond_to do |format|
      @products.each do |product|
        product.picture = root_url+'product/'+product.picture if !product.picture.blank?
      end
      format.json { render json: @products }
    end
  end

  def load_auto_complete_search_products
    if !params[:text].blank?
      @products = Product.active.search_scope(params[:text])
    end
    respond_to do |format|
      format.js
      format.json { render json: {:product_ids => @products.collect { |product| product.id }.uniq} }
    end
  end

  def clear_auto_complete_text

  end

  def show_search_products
    @products = Product.active.search_scope(params[:text])
  end

  def product_details_tab
    @product = Product.find(params[:id])
    @brand = Brand.find(@product.brand_id)
  end

  def product_request
    @product_request = ProductRequest.new
    @product_request.product_name = params[:product_name]
    @product_request.customer_name = params[:customer_name]
    @product_request.email = params[:email]
    @product_request.phone_number = params[:phone_number]
    @product_request.save
    redirect_to root_url, :notice => 'Your request has been taken. we will contact soon.'
  end

  def request_products
    @product_requests = ProductRequest.find(:all, :order => 'updated_at DESC')
  end

  def get_all_filtered_products
    category_ids = !params[:category_ids].blank? ? params[:category_ids].split(',') : params[:category_ids]
    if !params[:load_all].blank? && params[:load_all] && !params[:category_ids].blank?
      categories = []
      CategoriesHelper.get_categories_by_parent(params[:category_ids], categories)
      category_ids= categories.collect { |category| category.id.to_i }
      category_ids<<params[:category_ids].to_i
    end
    all_brand_ids = request.format != 'json' || params[:brand_ids].blank? ? params[:b] : params[:brand_ids].split(',')
    all_price_range_ids = request.format != 'json' || params[:price_range_ids].blank? ? params[:p] : params[:price_range_ids].split(',')
    limit = 20
    offset = 0
    limit = params[:limit].to_i if !params[:limit].blank?
    offset = params[:offset].to_i if !params[:offset].blank?
    @all_products_size = Product.active.category_scope(category_ids).brand_scope(all_brand_ids).price_scope(all_price_range_ids).sort_scope(params[:sort_key]).size
    @products = Product.active.category_scope(category_ids).brand_scope(all_brand_ids).price_scope(all_price_range_ids).sort_scope(params[:sort_key]).limit(limit).offset(offset)
    @product_brand_ids = Product.active.category_scope(category_ids).brand_scope(all_brand_ids).price_scope(all_price_range_ids).select('DISTINCT(brand_id)')
    @product_price_range_ids = Product.active.category_scope(category_ids).brand_scope(all_brand_ids).price_scope(all_price_range_ids).select('DISTINCT(product_price_range_id)')
    brand_ids = @product_brand_ids.collect { |brand_id_item| brand_id_item.brand_id }.uniq
    @brands = Array.new
    brand_ids.each do |brand_id|
      brand = Brand.active.find_by_id(brand_id)
      @brands << brand if !brand.blank?
    end
    @price_range_ids = @product_price_range_ids.collect { |product| product.product_price_range_id }.uniq
    @price_range_ids = @price_range_ids.sort! { |a, b| a <=> b }
    @brands = @brands.sort! { |a, b| a.name <=> b.name }
  end

  def clean_orders
    ActiveRecord::Base.connection.execute('delete from cart_items where item_id not in (select id from products)');
    ActiveRecord::Base.connection.execute('delete from order_items where product_id not in (select id from products)');
    redirect_to root_url, :notice => 'Deleted products are cleared'
  end

  def change_product_selection_quantity
    @product = Product.find_by_id(params[:product_id])
  end

  def change_product_tab_selection_quantity
    redirect_to "/products/product_details_tab?id=#{params[:product_id]}"
  end

  def edit_product
    @product = Product.find_by_id(params[:id])
    @product.name = params[:name]
    @product.price = params[:prices]
    @product.quantity = params[:quantity]
    @product.discount_value = params[:discount_value]
    @product.discount_type = params[:discount_type]
    @product.save
    @product.picture=root_url+'product/'+@product.picture if !@product.picture.blank?
    respond_to do |format|
      format.js
    end

  end

  def add_items_to_wish_list
    if current_user.blank?
      redirect_to '/user_sessions/new', :notice => 'Please sign in for adding items to your wish list.'

    else
      @product = Product.find_by_id(params[:product_id])
      @wish_list =WishList.new
      @wish_list.product_id=@product.id
      @wish_list.user_id = current_user.id
      @wish_list.save
    end
    respond_to do |format|
      format.js
      format.json { render json: @wish_list }
    end
  end

  def show_wish_list_products
    @products = Product.where('id in (select product_id from wish_lists where user_id =?)', current_user.id)
    respond_to do |format|
      format.html
      format.json { render json: @products }
    end
  end

  def remove_wish_list_items
    @status = 'Failed'
    @product = Product.find_by_id(params[:product_id])
    if !@product.blank?
      @wish_list = WishList.find_by_product_id_and_user_id(@product.id, current_user.id)
      if !@wish_list.blank?
        @wish_list.destroy
        @status = 'Success'
      end
    end
    respond_to do |format|
      format.js
      format.json { render :json => {:status => @status} }
    end
  end

  def load_more
    category_ids = !params[:category_ids].blank? ? params[:category_ids].split(',') : params[:category_ids]
    if !params[:load_all].blank? && params[:load_all] && !params[:category_ids].blank?
      categories = []
      CategoriesHelper.get_categories_by_parent(params[:category_ids], categories)
      category_ids= categories.collect { |category| category.id.to_i }
      category_ids<<params[:category_ids].to_i
    end
    all_brand_ids = params[:b]
    all_price_range_ids =params[:p]
    limit = 20
    offset = 0
    limit = params[:limit].to_i if !params[:limit].blank?
    offset = params[:offset].to_i if !params[:offset].blank?
    @products = Product.active.category_scope(category_ids).brand_scope(all_brand_ids).price_scope(all_price_range_ids).sort_scope(params[:sort_key]).limit(limit).offset(offset)
    @all_products_size=params[:all_products_size].to_i


  end

end


