class PasswordResetsController < ApplicationController
  include PasswordResetsHelper
  include UserSessionsHelper
  skip_before_filter :authorize
  before_filter :load_user_using_perishable_token, :only => [:edit, :update]
  def new
    if !current_user.nil?
      @user_session = UserSession.find
      if !@user_session.nil?
        @user_session.destroy
      end
    end
    render
  end

  def create
    @user = User.find_by_email(params[:email])
    if @user
      @user.deliver_password_reset_instructions!
      @user.save
      OrderMailer.password_reset_instructions(@user).deliver
      flash[:error] = %Q[Instructions to reset your password have been emailed to you.  Please check your email (Inbox and Spam folder).].html_safe
      redirect_to login_url
    else
      flash[:error] = "No user was found with that email address"
      render :action => :new
    end
  end

  def edit
    @password = ""
    @confirm_password = ""
    @reset_password_errors = ""
    render
  end

  def update
    @password = params[:password]
    @confirm_password = params[:password_confirmation]
    is_valid = validate_password(@password, @confirm_password)
    if is_valid
      @user.update_attribute(:password, @password)
      @user.update_attribute(:password_confirmation, @confirm_password)
      flash[:notice] = "Password successfully updated"
      redirect_to root_url
    else
      respond_to do |format|
        format.html { render action: "edit" }
     end
    end
  end

  private
  def load_user_using_perishable_token
    @user = User.find_using_perishable_token(params[:id])
    unless @user
      flash[:notice] = "We're sorry, but we could not locate your account. If you are having issues try copying and pasting the URL " +
          "from your email into your browser or restarting the reset password process."
      redirect_to root_url
    end
  end
end
