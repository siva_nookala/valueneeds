require 'test_helper'

class NotifyMeRequestsControllerTest < ActionController::TestCase
  setup do
    @notify_me_request = notify_me_requests(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:notify_me_requests)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create notify_me_request" do
    assert_difference('NotifyMeRequest.count') do
      post :create, notify_me_request: @notify_me_request.attributes
    end

    assert_redirected_to notify_me_request_path(assigns(:notify_me_request))
  end

  test "should show notify_me_request" do
    get :show, id: @notify_me_request.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @notify_me_request.to_param
    assert_response :success
  end

  test "should update notify_me_request" do
    put :update, id: @notify_me_request.to_param, notify_me_request: @notify_me_request.attributes
    assert_redirected_to notify_me_request_path(assigns(:notify_me_request))
  end

  test "should destroy notify_me_request" do
    assert_difference('NotifyMeRequest.count', -1) do
      delete :destroy, id: @notify_me_request.to_param
    end

    assert_redirected_to notify_me_requests_path
  end
end
