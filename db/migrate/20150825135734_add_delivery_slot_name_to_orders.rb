class AddDeliverySlotNameToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :delivery_slot_name, :string
  end
end
