class AddressDetail < ActiveRecord::Base
  validates_presence_of :first_name, :last_name, :contact_number, :house_no, :area, :pincode, :city, :land_mark

end
