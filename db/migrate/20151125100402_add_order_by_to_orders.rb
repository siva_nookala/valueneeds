class AddOrderByToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :order_by, :string
  end
end
