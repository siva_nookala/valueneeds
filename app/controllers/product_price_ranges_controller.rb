class ProductPriceRangesController < ApplicationController
  filter_resource_access
  filter_access_to :get_product_price_range, :attribute_check => false
  filter_access_to :load_product_price_range_ids, :attribute_check => false
  # GET /product_price_ranges
  # GET /product_price_ranges.json
  def index
    @product_price_ranges = ProductPriceRange.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @product_price_ranges }
    end
  end

  # GET /product_price_ranges/1
  # GET /product_price_ranges/1.json
  def show
    @product_price_range = ProductPriceRange.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @product_price_range }
    end
  end

  # GET /product_price_ranges/new
  # GET /product_price_ranges/new.json
  def new
    @product_price_range = ProductPriceRange.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @product_price_range }
    end
  end

  # GET /product_price_ranges/1/edit
  def edit
    @product_price_range = ProductPriceRange.find(params[:id])
  end

  # POST /product_price_ranges
  # POST /product_price_ranges.json
  def create
    @product_price_range = ProductPriceRange.new(params[:product_price_range])

    respond_to do |format|
      if @product_price_range.save
        format.html { redirect_to @product_price_range, notice: 'Product price range was successfully created.' }
        format.json { render json: @product_price_range, status: :created, location: @product_price_range }
      else
        format.html { render action: "new" }
        format.json { render json: @product_price_range.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /product_price_ranges/1
  # PUT /product_price_ranges/1.json
  def update
    @product_price_range = ProductPriceRange.find(params[:id])

    respond_to do |format|
      if @product_price_range.update_attributes(params[:product_price_range])
        format.html { redirect_to @product_price_range, notice: 'Product price range was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @product_price_range.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /product_price_ranges/1
  # DELETE /product_price_ranges/1.json
  def destroy
    @product_price_range = ProductPriceRange.find(params[:id])
    @product_price_range.destroy

    respond_to do |format|
      format.html { redirect_to product_price_ranges_url }
      format.json { head :ok }
    end
  end

  def load_product_price_range_ids
    @product_price_ranges = ProductPriceRange.all
    @product_price_ranges.each do |product_price_range|
      Product.where('price >= ? and price <= ?', product_price_range.start_price, product_price_range.end_price).update_all(product_price_range_id: product_price_range.id)
    end
    redirect_to '/product_price_ranges'
  end

  def get_product_price_range
    @product_price_ranges = ProductPriceRange.find(:all, :conditions => ['id in (?)', params[:price_range_ids].split(',')])
    respond_to do |format|
      format.json { render json: @product_price_ranges}
    end
  end
end
