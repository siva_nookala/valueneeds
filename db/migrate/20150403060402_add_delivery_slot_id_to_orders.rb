class AddDeliverySlotIdToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :delivery_slot_id, :integer
  end
end
