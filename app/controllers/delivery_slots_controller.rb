class DeliverySlotsController < ApplicationController
  filter_resource_access
  filter_access_to :update_delivery_slot, :attribute_check => false
  filter_access_to :create_delivery_slots, :attribute_check => false
  include ApplicationHelper

  def index
    @delivery_slots = DeliverySlot.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @delivery_slots }
    end
  end

  # GET /delivery_slots/1
  # GET /delivery_slots/1.json
  def show
    @delivery_slot = DeliverySlot.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @delivery_slot }
    end
  end

  # GET /delivery_slots/new
  # GET /delivery_slots/new.json
  def new
    @delivery_slot = DeliverySlot.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @delivery_slot }
    end
  end

  # GET /delivery_slots/1/edit
  def edit
    @delivery_slot = DeliverySlot.find(params[:id])
  end

  # POST /delivery_slots
  # POST /delivery_slots.json
  def create
    @delivery_slot = DeliverySlot.new(params[:delivery_slot])

    respond_to do |format|
      if @delivery_slot.save
        format.html { redirect_to @delivery_slot, notice: 'Delivery slot was successfully created.' }
        format.json { render json: @delivery_slot, status: :created, location: @delivery_slot }
      else
        format.html { render action: "new" }
        format.json { render json: @delivery_slot.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /delivery_slots/1
  # PUT /delivery_slots/1.json
  def update
    @delivery_slot = DeliverySlot.find(params[:id])

    respond_to do |format|
      if @delivery_slot.update_attributes(params[:delivery_slot])
        format.html { redirect_to @delivery_slot, notice: 'Delivery slot was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @delivery_slot.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_delivery_slot
    if !params[:delivery_slot].blank?
      session[ApplicationHelper::SessionConstants::BUYER_LOCATION] = params[:location]
    end
    redirect_to delivery_slots_url
  end

  # DELETE /delivery_slots/1
  # DELETE /delivery_slots/1.json
  def destroy
    @delivery_slot = DeliverySlot.find(params[:id])
    @delivery_slot.destroy

    respond_to do |format|
      format.html { redirect_to delivery_slots_url }
      format.json { head :ok }
    end
  end

  def create_delivery_slots
    (get_today_date.. get_today_date+30).each do |date|
      get_every_day_slots.each do |slot|
        slot_name = get_today_date1(date).to_s+' '+slot[0]
        delivery_slot_name = DeliverySlotName.find_by_slot_name(slot_name)
        if delivery_slot_name.blank?
          @delivery_slot_name = DeliverySlotName.new
          @delivery_slot_name.slot_name = slot_name
          @delivery_slot_name.dont_show_after = date.to_date + slot[1].hours - slot[2].hours
          @delivery_slot_name.active = true
          @delivery_slot_name.save
        end
      end
    end
    redirect_to "/delivery_slot_names"
  end

end
