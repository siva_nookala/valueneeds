class ChangePriceInProducts < ActiveRecord::Migration
  def up
    change_column :products, :price, :decimal
    change_column :product_prices, :price, :decimal
    change_column :product_prices, :discount, :decimal
    change_column :order_items, :price, :decimal
    change_column :order_items, :discount_price, :decimal
    change_column :order_items, :total_price, :decimal
    change_column :order_items, :effective_price, :decimal
    change_column :order_items, :savings_amount, :decimal
  end

  def down
    change_column :products, :price, :string
    change_column :product_prices, :price, :string
    change_column :product_prices, :discount, :string
    change_column :order_items, :price, :string
    change_column :order_items, :discount_price, :string
    change_column :order_items, :total_price, :string
    change_column :order_items, :effective_price, :string
    change_column :order_items, :savings_amount, :string
  end
end


