module ApplicationHelper

  module SessionConstants
    SESSION_ORDER_ITEMS = :session_order_items
    BUYER_LOCATION = :buyer_location
    SESSION_ID = :session_id
  end

  def get_total_amount
    total_price = get_total_charges_with_out_delivery_charges
    delivery_charges = get_delivery_charges(total_price)
    if total_price>0
      total_amount = (total_price + delivery_charges).round(2)
    end
    total_amount
  end

  def get_total_charges_with_out_delivery_charges
    total_price = 0.0
    @total_cart_items = !current_user.blank? ? CartItem.find_all_by_user_id(current_user.id) : CartItem.find_all_by_session_id(session[SessionConstants::SESSION_ID])
    category_lists = get_cart_by_category_level(@total_cart_items)
    category_lists.each do |category_list|
      cart_items = category_list.cart_order_items
      promo_range = category_list.promo
      cart_items.each_with_index do |item, index|
        order_item = OrderItem.new
        order_item.product_id = item.order_item.product_id
        order_item.quantity = item.order_item.quantity
        order_item.price =item.order_item.original_price.to_d
        order_item.product_attributes = item.order_item.product_attributes if !item.order_item.product_attributes.blank?
        order_item.effective_price = item.order_item.effective_price.to_d
        order_item.total_price = item.order_item.sub_total
        order_item.savings_amount = (order_item.price - order_item.effective_price)*order_item.quantity.to_i
        if !item.promo.blank?
          order_item.promo_id = item.promo.promo_id
          order_item.promo_quantity = item.promo.offer_limit_quantity
          order_item.promo_offer_type = item.promo.offer_type
          order_item.savings_amount = item.promo.savings_amount
        end
        if !promo_range.blank? && index+1 == cart_items.size
          order_item.promo_id = promo_range.promo_id
          order_item.promo_quantity = promo_range.offer_limit_quantity
          order_item.promo_offer_type = promo_range.offer_type
          order_item.savings_amount = promo_range.savings_amount
        end
        total_price = total_price + order_item.total_price.to_d.round(2)
      end
    end
    total_price
  end


  def get_categories
    categories = Category.all
    list_of_categories = Array.new
    categories.each do |category|
      list_of_categories << [category.name, category.id]
    end
    list_of_categories
  end

  def get_locations
    locations = Location.all
    list_of_locations = Array.new
    locations.each do |location|
      list_of_locations << [location.name, location.id]
    end
    list_of_locations
  end

  def get_promo_groups
    promo_groups = PromoGroup.find_all_by_active(true)
    list_of_groups = Array.new
    promo_groups.each do |promo_group|
      list_of_groups << [promo_group.title, promo_group.id]
    end
    list_of_groups
  end

  def get_delivery_slots
    delivery_slots = DeliverySlot.all
    list_of_delivery_slots = Array.new
    delivery_slots.each do |delivery_slot|
      list_of_delivery_slots << ["#{delivery_slot.start_date_time.strftime('%A %-l:%M %p')} - #{delivery_slot.end_date_time.strftime('%-l:%M %p')}", delivery_slot.id]
    end
    list_of_delivery_slots
  end


  def get_colours
    ['#5C6CC4', '#73C45D', '#E1A810', '#E15011', '#41C2E2', '#D76ECD', '#4ED497', '#5A9CF3']
  end

  def get_add_button
    "<span>ADD</span><span class=\"icon icon-basket-gray\"></span>".html_safe
  end

  def get_matched_product(order_items, item_id)
    result=nil
    if !order_items.blank?
      order_items.each do |item|
        result = item if item.item_id == item_id
      end
    end
    result
  end

  def filtered_products_by_category(category_id)
    categories_lists = get_all_subcategories(category_id, Array.new)
    categories_lists << params[:cI]
    products_lists = get_all_products_for_category(categories_lists)
    products = Set.new
    products_lists.each do |product_id|
      product = Product.find_by_id(product_id)
      products << product if !product.blank?
    end
    products
  end


  def get_all_subcategories(category_id, result_list)
    categories = Category.find_all_by_parent_id(category_id)
    categories.each do |category|
      result_list << category.id
      if !result_list.blank? && result_list.size > 0
        get_all_subcategories(category.id, result_list)
      end
    end
    result_list
  end

  def get_all_products_for_category(categories_list)
    products_lists = Array.new
    categories_list.each do |category_id|
      product_categories = ProductCategory.find_all_by_category_id(category_id)
      product_categories.each do |product_category|
        products_lists << product_category.product_id if !product_category.blank?
      end
    end
    products_lists
  end

  def get_all_brands_for_products(products)
    brands = Set.new
    products.each do |product|
      brands << Brand.find(product.brand_id) if !product.blank?
    end
    brands
  end

  def get_all_brands
    brands_list = Array.new
    brands = Brand.all
    brands.each do |brand|
      brands_list << [brand.name, brand.id] if !brand.blank?
    end
    brands_list
  end

  def get_all_discount_type
    ['No-Discount', 'Discount-Percentage', 'Discount-Value']
  end


  def get_category_path(category)
    path = "<a href='#{root_url}' class=\"path_categories\">Home</a>"
    category_lists = Array.new
    category_lists << category
    if !category.blank?
      get_all_parent_categories(category, category_lists)
    end
    category_lists.reverse.each do |category|
      path = path + ">" + "<a href=\"/products/get_main_filtered_products?category_ids=#{category.id}&load_all=true&rF=true\"  class=\"path_categories\">#{category.name}</a>" if !category.blank?
    end
    path.html_safe
  end

  def get_all_parent_categories(category, category_lists)
    parent_category = Category.find_by_id(category.parent_id)
    category_lists << parent_category if !parent_category.blank?
    if !parent_category.blank? && !parent_category.parent_id.blank?
      get_all_parent_categories(parent_category, category_lists)
    end
    category_lists
  end

  def show_sub_categories(category_id)
    categories = Category.active.find_all_by_parent_id(category_id)
    sub_category_list = ""
    indent = 1
    category = Category.active.find_by_id(category_id)
    if !category.blank?
      result = categories.blank? ? Array.new : Array.new << category
      categories = Category.active.find_all_by_parent_id(category.parent_id) if categories.blank?
      categories = categories.sort! { |a, b| a.name <=> b.name }
      parent_categories = get_all_parent_categories(category, result).reverse
      parent_categories.each do |category|
        sub_category_list = sub_category_list+ "<div class=\"category_highlight\">
                          <a href=\"/products/get_products?category_ids=#{category.id}&load_all=true&rF=true\"  data-remote=\"true\"> #{category.name}</a>
                         </div>"
        indent = indent+1
      end
    end
    sub_category_list += "<div style=\"border:1px solid #e3e2e0;border-bottom:none\">"
    categories.each_with_index do |category, index|
      color = category.id == category_id ? 'green' : ''
      sub_category_list = sub_category_list+"<div class=\"sub_category_highlight\">
                          <a href=\"/products/get_products?category_ids=#{category.id}&load_all=true&rF=true\" style=\"color:#{color}\" data-remote=\"true\"> #{category.name}</a>
                         </div>"
    end
    sub_category_list += '</div>'
    sub_category_list.html_safe
  end

  def get_padding_categories(indent)
    indent*15
  end

  def get_html_list_for_all_sub_categories(category, result)
    result<< "<li>"
    sub_categories = Category.active.find(:all, :conditions => ['parent_id =?', category.id], :order => "sequence_id ASC")
    add_class = !sub_categories.blank? && sub_categories.size > 0 ? 'has_categories' : ''
    result<<"<a href='/products/get_main_filtered_products?category_ids=#{category.id}&load_all=true&rF=true' class='category_list #{add_class}'>#{category.name }</a>"
    # if !sub_categories.blank? && sub_categories.size > 0
    #    result<<"<ul>"
    #     sub_categories.each do |sub_category|
    #       get_html_list_for_all_sub_categories(sub_category, result)
    #     end
    #    result<<"</ul>"
    # end
    result<<"</li>"
  end

  def get_html_list_for_all_categories(category, result)

    result<< "<li>"

    sub_categories = Category.active.find(:all, :conditions => ['parent_id =?', category.id], :order => "sequence_id ASC")
    add_class = !sub_categories.blank? && sub_categories.size > 0 ? 'has_categories' : ''
    result<<"<a href='/products/get_main_filtered_products?category_ids=#{category.id}&load_all=true&rF=true' class='category_list #{add_class}'>#{category.name }</a>"
    if !sub_categories.blank? && sub_categories.size > 0
      result<<"<li>"
      sub_categories.each do |sub_category|
        get_html_list_for_all_sub_categories(sub_category, result)
      end
      result<<"</li>"
    end
    result<<"</li>"
  end

  def get_discount_product_price(product)
    result = 0
    if !product.discount_type.blank? && !product.discount_type.eql?('No-Discount') && !product.discount_value.blank?
      result = product.discount_type.eql?('Discount-Percentage') ? (product.price/100)*product.discount_value.to_f : product.discount_value.to_f
      result = result.round(2)
    end
    result
  end

  def get_formatted_date(date)
    result = ''
    if !date.blank?
      result = date.strftime("%A %-l:%M %p")
    end
    result

  end

  def get_formatted_date2(date)
    result = ''
    if !date.blank?
      result = date.strftime("%-l:%M %p")
    end
    result

  end

  def get_formatted_date1(date)
    result = ''
    if !date.blank?
      result = date.strftime("%Y-%m-%d %H:%M:%S")
    end
    result

  end

  def get_formatted_date3(date)
    result = ''
    if !date.blank?
      result = date.utc.in_time_zone('Kolkata').strftime("%d-%m-%Y %-l:%M %p")
    end
    result

  end

  def number_format_price(price)
    number_with_precision(price, precision: 2, :delimiter => ',')
  end

  def send_sms(mobile_number, original)
    message = original.dup
    if mobile_number.to_s.strip.length == 10
      message.gsub!('%', "%25")
      message.gsub!('&', "%26")
      message.gsub!('+', "%2B")
      message.gsub!('#', "%23")
      message.gsub!('=', "%3D")
      message.gsub!(' ', "%20")
      url = "http://t.smsap.com/api/web2sms.php?workingkey=A068ceefc300d983dc777b795aa7f579d&sender=VNEEDS&to=#{mobile_number}&message=#{message}"
      %x[curl "#{url}"]
    end
  end

  def get_delivery_charges(total_price)
    result = SystemSetting.find_by_key('delivery_charges').value.to_i
    if total_price > SystemSetting.find_by_key('free_delivery_amount').value.to_i
      result = 0
    end
    result
  end

  def is_valid_coupon(user_coupon_code, actual_coupon_code)
    order = Order.find_by_buyer_id(current_user.id)
    return !user_coupon_code.blank? && (user_coupon_code.eql?(actual_coupon_code) && order.blank?)
  end

  def get_discount_on_coupon(amount)
    [200, amount*0.2].min.round
  end

  def cc_avenue_details(payment_details, return_url)
    address_details = AddressDetail.find_by_id(payment_details.address_id)
    full_address = address_details.house_no + '-' + address_details.land_mark + '-' + address_details.area
    values = {
        :order_id => payment_details.merchant_transaction_id,
        :amount => payment_details.gateway_amount,
        :currency => 'INR',
        :merchant_id => '76002',
        :redirect_url => return_url,
        :cancel_url => return_url,
        :language => 'EN',
        :billing_name => address_details.first_name + ' ' + address_details.last_name,
        :billing_address => full_address,
        :billing_city => address_details.city,
        :billing_zip => address_details.pincode,
        :billing_tel => address_details.contact_number,
        :billing_email => current_user.email,
        :billing_state => 'AP',
        :billing_country => 'India'
    }
    "http://www.valueneeds.com/static/ccavRequestHandler?" + values.to_query
  end

  def needs_payment?(cart_items)
    result = false
    cart_items.each do |cart_item|
      product = Product.find_by_id(cart_item.item_id)
      if product.needs_payment
        result = true
      end
    end
    result
  end

  def get_category_content(children_list, level)
    result=''
    children_list.each do |child_category|
      sub_children_list=Category.find_all_by_parent_id(child_category.id)
      if !sub_children_list.blank?
        result+="<div style='padding-left:#{level*40}px;'><div class='col-sm-12 category_box_shadow_style'>
        <table width='100%'><tr><td width='80%'><span class='category_title_style'>#{child_category.name}</span></td><td align='right'><div class='text-right', onclick=\"show_or_hide('ch_#{child_category.id }');\">#{image_tag('plus.png', :width => '20px;', :id => "ch_#{child_category.id}_img")}</div></td></tr></table>
              </div></div><div id='ch_#{child_category.id}' style='display:none' data_open='false'>"
        result+=get_category_content(sub_children_list, level+1)
        result+='</div>'
      else
        result+="<a href='/products/get_main_filtered_products?category_ids=#{child_category.id}&load_all=true&rF=true'><div style='padding-left:#{level*40}px;'><div class='col-sm-12 category_box_shadow_style'>
      <span class='category_title_style'>#{child_category.name}</span>
              </div></div></a>"
      end
    end
    result
  end

  def get_today_date
    Date.today
  end

  def get_today_date1(date)
    result = ''
    if !date.blank?
      result = date.strftime("%-d %b")
    end
    result
  end

  def get_every_day_slots
    [["7 am - 10 am", 7, 10], ["10 am - 1 pm", 10, 1], ["2 pm - 5 pm", 14, 1], ["5 pm - 7 pm", 17, 1], ["7 pm - 10 pm", 19, 1]]
  end
end
