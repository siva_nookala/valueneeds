class AddLargePictureToProducts < ActiveRecord::Migration
  def change
    add_column :products, :large_picture, :string
  end
end
