class CreateFeedbacks < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
      t.string :user_name
      t.string :email
      t.string :comment
      t.string :mobile_number

      t.timestamps
    end
  end
end
