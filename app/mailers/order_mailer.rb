class OrderMailer < ActionMailer::Base
  default from: "\"ValueNeeds \" <valeeds123@gmail.com>"
  default_url_options[:host] = "www.valueneeds.com"

  def send_status_mail(user, order, emails)
    @user = user
    @order = order
    mail(:to => @user.email, :subject => 'Thanks for your order - ValueNeeds', :cc => 'valeeds123@gmail.com,' + emails)

  end

  def send_feed_back_mail(feedback, emails)
    @feedback = feedback

    mail(:to => "valeeds123@gmail.com", :subject => 'User Feedback', :cc => @feedback.email+',' + emails)
  end

  def password_reset_instructions(user)
    @user = user
    mail(:to => user.email, :subject => user.user_name.to_s+' - Value Needs - Reset Password')
  end
end
