require 'test_helper'

class PromoGroupsControllerTest < ActionController::TestCase
  setup do
    @promo_group = promo_groups(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:promo_groups)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create promo_group" do
    assert_difference('PromoGroup.count') do
      post :create, promo_group: @promo_group.attributes
    end

    assert_redirected_to promo_group_path(assigns(:promo_group))
  end

  test "should show promo_group" do
    get :show, id: @promo_group.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @promo_group.to_param
    assert_response :success
  end

  test "should update promo_group" do
    put :update, id: @promo_group.to_param, promo_group: @promo_group.attributes
    assert_redirected_to promo_group_path(assigns(:promo_group))
  end

  test "should destroy promo_group" do
    assert_difference('PromoGroup.count', -1) do
      delete :destroy, id: @promo_group.to_param
    end

    assert_redirected_to promo_groups_path
  end
end
