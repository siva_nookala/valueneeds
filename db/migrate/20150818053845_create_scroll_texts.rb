class CreateScrollTexts < ActiveRecord::Migration
  def change
    create_table :scroll_texts do |t|
      t.text :description

      t.timestamps
    end
  end
end
