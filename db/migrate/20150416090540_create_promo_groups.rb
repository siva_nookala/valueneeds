class CreatePromoGroups < ActiveRecord::Migration
  def change
    create_table :promo_groups do |t|
      t.string :title
      t.string :description
      t.string :image
      t.boolean :active

      t.timestamps
    end
  end
end
