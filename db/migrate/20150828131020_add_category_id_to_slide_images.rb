class AddCategoryIdToSlideImages < ActiveRecord::Migration
  def change
    add_column :slide_images, :category_id, :integer
  end
end
