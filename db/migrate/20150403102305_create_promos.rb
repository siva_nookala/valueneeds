class CreatePromos < ActiveRecord::Migration
  def change
    create_table :promos do |t|
      t.string :promo_type
      t.string :promo_title
      t.integer :product_id
      t.string :product_quantity
      t.integer :product_range_id
      t.integer :order_value
      t.datetime :valid_from
      t.datetime :valid_till
      t.integer :order_promo_limit
      t.integer :customer_promo_limit
      t.string :offer_type
      t.integer :discount_amount
      t.integer :discount_percent
      t.integer :offer_product_id
      t.string :offer_other_product_quantity
      t.string :offer_same_product_quantity

      t.timestamps
    end
  end
end
