Valueneeds::Application.routes.draw do

  resources :bottom_banners

  resources :password_resets

  resources :recipes do
    get :recipes_description, :on => :collection
  end

  resources :galleries do
  get :create_gallery, :on => :collection
  end
  resources :wish_lists

  resources :wallet_entries do
    get :my_wallet, :on => :collection
    get :show_wallet, :on => :collection
  end

  resources :notify_me_requests

  resources :delivery_slot_names do
    get :slots_for_mobile, :on => :collection
  end

  resources :pincodes do
    get :users_index, :on => :collection
  end

  resources :system_settings

  resources :product_attributes

  resources :attribute_items

  resources :attributes

  resources :scroll_texts

  resources :specialities

  get "static/contact_us"
  get "static/about_us"
  get "static/faq"
  get "static/privacy_policy"
  get "static/terms_and_conditions"
  get "static/delivery_information"
  get "static/refund_and_return_policy"
  get "static/follow_us"
  get "static/site_map"
  get "static/where_we_deliver"
  get "static/general_information"
  get "static/need_assistance"
  get "static/customer_service"
  get "static/getting_started"
  get "static/vn_offers"
  get "static/coupon_and_vouchers"
  get "static/news_and_events"
  get "static/winners"
  get "static/dataFrom"
  get "static/ccavRequestHandler"
  post "static/payment_response_handler"
  resources :slide_images

  resources :user_sessions


  resources :promo_groups

  resources :product_price_ranges do
    get :load_product_price_range_ids, :on => :collection
    get :get_product_price_range, :on => :collection
  end

  resources :promos do
    get :load_all_promo_ids, :on => :collection
    get :get_promos_by_group, :on => :collection
    get :promo_offers, :on => :collection
    get :promo_complete_details, :on => :collection
    get :get_promo_offer_details, :on => :collection
  end

  resources :delivery_slots do
  get :create_delivery_slots, :on => :collection
  end


  resources :feedbacks

  resources :address_details do
    get :check_out, :on => :collection
    post :create_order_with_previous_address, :on => :collection
    get :get_address_details, :on => :collection
    get :get_delivery_slots, :on => :collection
    get :pay_with_wallet, :on => :collection
    get :confirm_with_wallet, :on => :collection
    post :create_order_with_new_address, :on => :collection
    get :create_order_from_payment_gateway, :on => :collection
  end

  resources :orders do
    get :get_previous_orders, :on => :collection
    get :repeat_order, :on => :collection
    get :invoice_status_details, :on => :collection
    get :order_tracking, :on => :collection
    get :status, :on => :collection
    get :payment_status, :on => :collection
    get :order_status_details, :on => :collection

  end

  resources :users do
    post :update_user_details, :on => :collection
  end

  resources :products do
    get :add_child_form, :on=> :collection
    post :update_category_child, :on => :collection
    get :remove_category, :on => :collection
    post :update_product_location_prices, :on => :collection
    get :add_product_location_price_form, :on => :collection
    get :remove_location_price, :on => :collection
    get :order_online, :on => :collection
    get :add_items_to_cart, :on => :collection
    get :remove_order_item, :on => :collection
    get :show_cart, :on => :collection
    get :login_screen, :on => :collection
    get :remove_order, :on => :collection
    get :update_location, :on => :collection
    get :empty_cart, :on => :collection
    get :get_products, :on => :collection
    get :get_products_by_ids, :on => :collection
    get :load_auto_complete_search_products, :on => :collection
    get :clear_auto_complete_text, :on => :collection
    get :get_main_filtered_products, :on => :collection
    get :show_search_products, :on => :collection
    get :product_details_tab, :on => :collection
    get :product_request, :on => :collection
    get :request_products, :on => :collection
    get :clean_orders, :on => :collection
    get :change_product_selection_quantity, :on => :collection
    get :change_product_tab_selection_quantity, :on => :collection
    get :edit_product, :on => :collection
    get :add_items_to_wish_list, :on => :collection
    get :show_wish_list_products, :on => :collection
    get :remove_wish_list_items, :on => :collection
    get :load_more, :on => :collection
  end

  resources :categories do
    get :get_child_categories, :on => :collection
    get :update_sequence_ids, :on => :collection
    get :change_category_sequence_order, :on => :collection
  end

  resources :locations

  resources :brands do
    get :get_brands_by_ids, :on => :collection
  end
  match 'promo-details/:title/:promo_id', :controller => 'promos', :action => 'promo_complete_details'
  match 'order-products', :controller => 'products', :action => 'order_online'
  match 'login', :controller => 'user_sessions', :action => 'new'
  match 'wallet', :controller => 'wallet_entries', :action => 'my_wallet'
  match 'logout', :controller => 'user_sessions', :action => 'destroy'
  match 'auth/:provider/callback', :to => 'user_sessions#log_in_with_others'
  match 'log_in_with_email', :to => 'user_sessions#log_in_with_email'
  match 'reset-password', :controller => 'password_resets', :action => 'new'

  #match 'auth/:provider/callback', to: 'user_sessions#'
  match 'auth/failure', to: redirect('/')

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
   root :to => 'products#order_online'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end
