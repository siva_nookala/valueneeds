require 'test_helper'

class ScrollTextsControllerTest < ActionController::TestCase
  setup do
    @scroll_text = scroll_texts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:scroll_texts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create scroll_text" do
    assert_difference('ScrollText.count') do
      post :create, scroll_text: @scroll_text.attributes
    end

    assert_redirected_to scroll_text_path(assigns(:scroll_text))
  end

  test "should show scroll_text" do
    get :show, id: @scroll_text.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @scroll_text.to_param
    assert_response :success
  end

  test "should update scroll_text" do
    put :update, id: @scroll_text.to_param, scroll_text: @scroll_text.attributes
    assert_redirected_to scroll_text_path(assigns(:scroll_text))
  end

  test "should destroy scroll_text" do
    assert_difference('ScrollText.count', -1) do
      delete :destroy, id: @scroll_text.to_param
    end

    assert_redirected_to scroll_texts_path
  end
end
