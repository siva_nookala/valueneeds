class ScrollTextsController < ApplicationController
  filter_resource_access
  # GET /scroll_texts
  # GET /scroll_texts.json
  def index
    @scroll_texts = ScrollText.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @scroll_texts }
    end
  end

  # GET /scroll_texts/1
  # GET /scroll_texts/1.json
  def show
    @scroll_text = ScrollText.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @scroll_text }
    end
  end

  # GET /scroll_texts/new
  # GET /scroll_texts/new.json
  def new
    @scroll_text = ScrollText.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @scroll_text }
    end
  end

  # GET /scroll_texts/1/edit
  def edit
    @scroll_text = ScrollText.find(params[:id])
  end

  # POST /scroll_texts
  # POST /scroll_texts.json
  def create
    @scroll_text = ScrollText.new(params[:scroll_text])

    respond_to do |format|
      if @scroll_text.save
        format.html { redirect_to @scroll_text, notice: 'Scroll text was successfully created.' }
        format.json { render json: @scroll_text, status: :created, location: @scroll_text }
      else
        format.html { render action: "new" }
        format.json { render json: @scroll_text.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /scroll_texts/1
  # PUT /scroll_texts/1.json
  def update
    @scroll_text = ScrollText.find(params[:id])

    respond_to do |format|
      if @scroll_text.update_attributes(params[:scroll_text])
        format.html { redirect_to @scroll_text, notice: 'Scroll text was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @scroll_text.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /scroll_texts/1
  # DELETE /scroll_texts/1.json
  def destroy
    @scroll_text = ScrollText.find(params[:id])
    @scroll_text.destroy

    respond_to do |format|
      format.html { redirect_to scroll_texts_url }
      format.json { head :ok }
    end
  end
end
