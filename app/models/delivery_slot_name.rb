class DeliverySlotName < ActiveRecord::Base
  validates_presence_of :slot_name

  def self.get_active_slots
    delivery_slot_names = DeliverySlotName.all

    delivery_slot_names.reject! do |delivery_slot_name|
      diff = time_diff(delivery_slot_name.dont_show_after, Time.now.utc.in_time_zone('Kolkata'))
      puts delivery_slot_name.dont_show_after.to_s + " " + Time.now.utc.in_time_zone('Kolkata').to_s + " " + diff.to_s + " " + delivery_slot_name.active.to_s
      result = !delivery_slot_name.active || diff < 330
      result
    end

    delivery_slot_names.first(5)
  end

  def self.time_diff(start_time, end_time)
    seconds_diff = (start_time - end_time).to_i
    minutes = seconds_diff / 60
    minutes
  end

end
