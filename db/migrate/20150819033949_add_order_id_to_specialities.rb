class AddOrderIdToSpecialities < ActiveRecord::Migration
  def change
    add_column :specialities, :order_id, :integer
  end
end
