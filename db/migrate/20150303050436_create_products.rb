class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.integer :brand_id
      t.string :picture
      t.string :price
      t.boolean :active

      t.timestamps
    end
  end
end
