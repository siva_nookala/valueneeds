class CreateRecipes < ActiveRecord::Migration
  def change
    create_table :recipes do |t|
      t.integer :category_id
      t.string :title
      t.text :ingredients
      t.text :procedure
      t.string :small_picture
      t.string :large_picture

      t.timestamps
    end
  end
end
