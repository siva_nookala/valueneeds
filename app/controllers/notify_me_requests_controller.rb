class NotifyMeRequestsController < ApplicationController
  filter_resource_access
  # GET /notify_me_requests
  # GET /notify_me_requests.json
  def index
    @notify_me_requests = NotifyMeRequest.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @notify_me_requests }
    end
  end

  # GET /notify_me_requests/1
  # GET /notify_me_requests/1.json
  def show
    @notify_me_request = NotifyMeRequest.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @notify_me_request }
    end
  end

  # GET /notify_me_requests/new
  # GET /notify_me_requests/new.json
  def new
    if current_user.blank?
      redirect_to '/products/login_screen'
    else
      @notify_me_request = NotifyMeRequest.new
      @product = Product.find(params[:product_id])
      @notify_me_request.product_id = params[:product_id].to_i
      respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @notify_me_request }
      end
    end
  end

  # GET /notify_me_requests/1/edit
  def edit
    @notify_me_request = NotifyMeRequest.find(params[:id])
  end

  # POST /notify_me_requests
  # POST /notify_me_requests.json
  def create
    @notify_me_request = NotifyMeRequest.new(params[:notify_me_request])
    @notify_me_request.user_id = current_user.id

    respond_to do |format|
      if @notify_me_request.save
        format.html { redirect_to '/', notice: 'Notify me request was successfully created.' }
        format.json { render json: @notify_me_request, status: :created, location: @notify_me_request }
      else
        format.html { render action: "new" }
        format.json { render json: @notify_me_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /notify_me_requests/1
  # PUT /notify_me_requests/1.json
  def update
    @notify_me_request = NotifyMeRequest.find(params[:id])

    respond_to do |format|
      if @notify_me_request.update_attributes(params[:notify_me_request])
        format.html { redirect_to @notify_me_request, notice: 'Notify me request was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @notify_me_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /notify_me_requests/1
  # DELETE /notify_me_requests/1.json
  def destroy
    @notify_me_request = NotifyMeRequest.find(params[:id])
    @notify_me_request.destroy

    respond_to do |format|
      format.html { redirect_to notify_me_requests_url }
      format.json { head :ok }
    end
  end
end
