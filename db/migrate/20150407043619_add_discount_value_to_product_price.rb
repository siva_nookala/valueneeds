class AddDiscountValueToProductPrice < ActiveRecord::Migration
  def change
    add_column :product_prices, :discount_value, :string
  end
end
