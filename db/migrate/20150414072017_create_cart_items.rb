class CreateCartItems < ActiveRecord::Migration
  def change
    create_table :cart_items do |t|
      t.string :session_id
      t.integer :user_id
      t.integer :item_id
      t.string :quantity
      t.decimal :price

      t.timestamps
    end
  end
end
