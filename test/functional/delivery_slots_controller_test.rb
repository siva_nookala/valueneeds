require 'test_helper'

class DeliverySlotsControllerTest < ActionController::TestCase
  setup do
    @delivery_slot = delivery_slots(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:delivery_slots)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create delivery_slot" do
    assert_difference('DeliverySlot.count') do
      post :create, delivery_slot: @delivery_slot.attributes
    end

    assert_redirected_to delivery_slot_path(assigns(:delivery_slot))
  end

  test "should show delivery_slot" do
    get :show, id: @delivery_slot.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @delivery_slot.to_param
    assert_response :success
  end

  test "should update delivery_slot" do
    put :update, id: @delivery_slot.to_param, delivery_slot: @delivery_slot.attributes
    assert_redirected_to delivery_slot_path(assigns(:delivery_slot))
  end

  test "should destroy delivery_slot" do
    assert_difference('DeliverySlot.count', -1) do
      delete :destroy, id: @delivery_slot.to_param
    end

    assert_redirected_to delivery_slots_path
  end
end
