class CreateProductPrices < ActiveRecord::Migration
  def change
    create_table :product_prices do |t|
      t.integer :product_id
      t.integer :location_id
      t.string :price
      t.string :discount
      t.string :valid_till
      t.boolean :active

      t.timestamps
    end
  end
end
