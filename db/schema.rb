# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20151019093506) do

  create_table "address_details", :force => true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "contact_number"
    t.string   "house_no"
    t.string   "area"
    t.string   "pincode"
    t.string   "city"
    t.string   "land_mark"
    t.integer  "buyer_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "attribute_items", :force => true do |t|
    t.integer  "attribute_id"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "attributes", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "brands", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "picture"
    t.boolean  "active"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cart_items", :force => true do |t|
    t.string   "session_id"
    t.integer  "user_id"
    t.integer  "item_id"
    t.string   "quantity"
    t.decimal  "price"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "product_attributes"
    t.string   "merchant_transaction_id"
  end

  create_table "categories", :force => true do |t|
    t.integer  "parent_id"
    t.string   "name"
    t.string   "picture"
    t.boolean  "active"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "sequence_id"
    t.text     "description"
    t.string   "html_title"
    t.string   "html_keywords"
    t.string   "html_description"
  end

  create_table "delivery_slot_names", :force => true do |t|
    t.string   "slot_name"
    t.boolean  "active"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "dont_show_after"
  end

  create_table "delivery_slots", :force => true do |t|
    t.datetime "start_date_time"
    t.datetime "end_date_time"
    t.boolean  "enable"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "feedbacks", :force => true do |t|
    t.string   "user_name"
    t.string   "email"
    t.string   "comment"
    t.string   "mobile_number"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "galleries", :force => true do |t|
    t.string   "title"
    t.datetime "date"
    t.string   "event_name"
    t.string   "picture"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "locations", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "picture"
    t.boolean  "active"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "notify_me_requests", :force => true do |t|
    t.integer  "product_id"
    t.integer  "user_id"
    t.string   "intimation_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "online_payments", :force => true do |t|
    t.string   "merchant_transaction_id"
    t.decimal  "transaction_amount"
    t.integer  "order_id"
    t.integer  "address_id"
    t.string   "payment_status"
    t.text     "transaction_details"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "wallet_amount"
    t.decimal  "gateway_amount"
    t.integer  "wallet_entry_id"
    t.integer  "user_id"
    t.decimal  "gateway_amount_recieved"
  end

  create_table "order_items", :force => true do |t|
    t.integer  "order_id"
    t.integer  "product_id"
    t.decimal  "price"
    t.decimal  "discount_price"
    t.string   "quantity"
    t.decimal  "total_price"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "effective_price"
    t.integer  "promo_id"
    t.integer  "promo_quantity"
    t.string   "promo_type"
    t.string   "promo_offer_type"
    t.decimal  "savings_amount"
    t.string   "product_attributes"
  end

  create_table "order_status_histories", :force => true do |t|
    t.integer  "order_id"
    t.integer  "user_id"
    t.string   "status_type"
    t.string   "old_status"
    t.string   "new_status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "orders", :force => true do |t|
    t.integer  "buyer_id"
    t.string   "payment_status"
    t.string   "status"
    t.string   "total_amount"
    t.string   "payment_mode"
    t.integer  "parent_id"
    t.integer  "address_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "location_id"
    t.integer  "delivery_slot_id"
    t.string   "delivery_slot_name"
    t.decimal  "delivery_charges"
    t.string   "tracking_id"
  end

  create_table "pincodes", :force => true do |t|
    t.string   "code"
    t.boolean  "active"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "product_attributes", :force => true do |t|
    t.integer  "product_id"
    t.integer  "attribute_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "product_categories", :force => true do |t|
    t.integer  "product_id"
    t.integer  "category_id"
    t.boolean  "actvie"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "product_price_ranges", :force => true do |t|
    t.decimal  "start_price"
    t.decimal  "end_price"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "product_prices", :force => true do |t|
    t.integer  "product_id"
    t.integer  "location_id"
    t.decimal  "price"
    t.decimal  "discount"
    t.string   "valid_till"
    t.boolean  "active"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "discount_value"
    t.string   "discount_type"
  end

  create_table "product_promo_mappings", :force => true do |t|
    t.integer  "product_id"
    t.integer  "promo_id"
    t.string   "promo_title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "product_requests", :force => true do |t|
    t.text     "product_name"
    t.string   "customer_name"
    t.string   "email"
    t.string   "phone_number"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "products", :force => true do |t|
    t.string   "name"
    t.integer  "brand_id"
    t.string   "picture"
    t.decimal  "price"
    t.boolean  "active"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "discount_type"
    t.string   "discount_value"
    t.integer  "product_price_range_id"
    t.string   "bb_product_id",          :limit => nil
    t.string   "quantity",               :limit => nil
    t.text     "description"
    t.boolean  "is_attribute",                          :default => false
    t.boolean  "out_of_stock",                          :default => false
    t.integer  "group_id"
    t.string   "html_title"
    t.string   "html_keywords"
    t.string   "html_description"
    t.boolean  "needs_payment",                         :default => false
    t.string   "large_picture"
    t.boolean  "pre_order",                             :default => false
    t.boolean  "is_hot_deal",                           :default => false
  end

  create_table "promo_groups", :force => true do |t|
    t.string   "title"
    t.string   "description"
    t.string   "image"
    t.boolean  "active"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "promos", :force => true do |t|
    t.string   "promo_type"
    t.string   "promo_title"
    t.integer  "product_id"
    t.string   "product_quantity"
    t.integer  "product_range_id"
    t.integer  "order_value"
    t.datetime "valid_from"
    t.datetime "valid_till"
    t.integer  "order_promo_limit"
    t.integer  "customer_promo_limit"
    t.string   "offer_type"
    t.integer  "discount_amount"
    t.integer  "discount_percent"
    t.integer  "offer_product_id"
    t.string   "offer_other_product_quantity"
    t.string   "offer_same_product_quantity"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "promo_group_id"
  end

  create_table "recipes", :force => true do |t|
    t.integer  "category_id"
    t.string   "title"
    t.text     "ingredients"
    t.text     "procedure"
    t.string   "small_picture"
    t.string   "large_picture"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "scroll_texts", :force => true do |t|
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "slide_images", :force => true do |t|
    t.string   "name"
    t.boolean  "active"
    t.string   "image"
    t.integer  "order_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "category_id"
  end

  create_table "specialities", :force => true do |t|
    t.string   "name"
    t.integer  "category_id"
    t.string   "image"
    t.boolean  "active"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "order_id"
  end

  create_table "system_settings", :force => true do |t|
    t.string   "key"
    t.text     "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_access_tokens", :force => true do |t|
    t.integer  "user_id"
    t.string   "access_token"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", :force => true do |t|
    t.string   "user_name"
    t.string   "email"
    t.string   "phone"
    t.string   "display_name"
    t.string   "persistence_token"
    t.string   "password_salt"
    t.string   "crypted_password"
    t.boolean  "admin"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "password"
    t.string   "password_confirmation"
    t.string   "perishable_token",      :default => "", :null => false
  end

  add_index "users", ["perishable_token"], :name => "index_users_on_perishable_token"

  create_table "wallet_entries", :force => true do |t|
    t.integer  "user_id"
    t.decimal  "amount"
    t.string   "reason"
    t.string   "transaction_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "order_id"
  end

  create_table "wish_lists", :force => true do |t|
    t.integer  "product_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
