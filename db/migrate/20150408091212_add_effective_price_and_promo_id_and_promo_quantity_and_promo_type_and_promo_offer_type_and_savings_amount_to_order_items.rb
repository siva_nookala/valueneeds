class AddEffectivePriceAndPromoIdAndPromoQuantityAndPromoTypeAndPromoOfferTypeAndSavingsAmountToOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :effective_price, :string
    add_column :order_items, :promo_id, :integer
    add_column :order_items, :promo_quantity, :integer
    add_column :order_items, :promo_type, :string
    add_column :order_items, :promo_offer_type, :string
    add_column :order_items, :savings_amount, :string
  end
end
