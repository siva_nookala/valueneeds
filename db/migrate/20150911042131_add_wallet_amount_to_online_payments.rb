class AddWalletAmountToOnlinePayments < ActiveRecord::Migration
  def change
    add_column :online_payments, :wallet_amount, :decimal
    add_column :online_payments, :gateway_amount, :decimal
    add_column :online_payments, :wallet_entry_id, :integer
    add_column :online_payments, :user_id, :integer
  end
end
