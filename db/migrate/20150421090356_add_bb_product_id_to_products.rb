class AddBbProductIdToProducts < ActiveRecord::Migration
  def change
    add_column :products, :bb_product_id, :String
    add_column :products, :quantity, :String
  end
end
