module PasswordResetsHelper
  def validate_password(password, confirm_password)
    result =  !password.empty? && !confirm_password.empty? && (password == confirm_password)
    @reset_password_errors = ''
    if password.empty?
      @reset_password_errors = 'Password should not be empty'
    elsif confirm_password.empty?
      @reset_password_errors = 'Confirm password should not be empty.'
    elsif (password != confirm_password)
      @reset_password_errors = 'Password and Confirm password should be same.'
    end
    result
  end
end
