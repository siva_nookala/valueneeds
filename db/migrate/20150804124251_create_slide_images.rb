class CreateSlideImages < ActiveRecord::Migration
  def change
    create_table :slide_images do |t|
      t.string :category_name
      t.boolean :active
      t.string :image
      t.integer :order

      t.timestamps
    end
  end
end
