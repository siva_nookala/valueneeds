require 'test_helper'

class WalletEntriesControllerTest < ActionController::TestCase
  setup do
    @wallet_entry = wallet_entries(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:wallet_entries)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create wallet_entry" do
    assert_difference('WalletEntry.count') do
      post :create, wallet_entry: @wallet_entry.attributes
    end

    assert_redirected_to wallet_entry_path(assigns(:wallet_entry))
  end

  test "should show wallet_entry" do
    get :show, id: @wallet_entry.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @wallet_entry.to_param
    assert_response :success
  end

  test "should update wallet_entry" do
    put :update, id: @wallet_entry.to_param, wallet_entry: @wallet_entry.attributes
    assert_redirected_to wallet_entry_path(assigns(:wallet_entry))
  end

  test "should destroy wallet_entry" do
    assert_difference('WalletEntry.count', -1) do
      delete :destroy, id: @wallet_entry.to_param
    end

    assert_redirected_to wallet_entries_path
  end
end
