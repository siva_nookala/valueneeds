class OrderItem < ActiveRecord::Base
  attr_accessor :product_name

  def as_json(options = { })
    h = super(options)
    h[:product_name]   = product_name
    h
  end
end
