require 'test_helper'

class AttributeItemsControllerTest < ActionController::TestCase
  setup do
    @attribute_item = attribute_items(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:attribute_items)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create attribute_item" do
    assert_difference('AttributeItem.count') do
      post :create, attribute_item: @attribute_item.attributes
    end

    assert_redirected_to attribute_item_path(assigns(:attribute_item))
  end

  test "should show attribute_item" do
    get :show, id: @attribute_item.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @attribute_item.to_param
    assert_response :success
  end

  test "should update attribute_item" do
    put :update, id: @attribute_item.to_param, attribute_item: @attribute_item.attributes
    assert_redirected_to attribute_item_path(assigns(:attribute_item))
  end

  test "should destroy attribute_item" do
    assert_difference('AttributeItem.count', -1) do
      delete :destroy, id: @attribute_item.to_param
    end

    assert_redirected_to attribute_items_path
  end
end
