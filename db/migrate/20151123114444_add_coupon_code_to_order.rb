class AddCouponCodeToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :coupon_code, :string
    add_column :orders, :coupon_discount, :decimal
    add_column :online_payments, :coupon_code, :string
    add_column :online_payments, :coupon_discount, :decimal
  end
end
