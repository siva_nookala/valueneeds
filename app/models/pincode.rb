class Pincode < ActiveRecord::Base
  validates_presence_of :code
end
