class RemoveOrderIdFromWalletEntries < ActiveRecord::Migration
  def up
    remove_column :wallet_entries, :order_id
  end

  def down
    add_column :wallet_entries, :order_id, :integer
  end
end
