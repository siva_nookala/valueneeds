class AddMerchantTransactionIdToCartItems < ActiveRecord::Migration
  def change
    add_column :cart_items, :merchant_transaction_id, :string
  end
end
