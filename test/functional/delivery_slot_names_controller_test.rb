require 'test_helper'

class DeliverySlotNamesControllerTest < ActionController::TestCase
  setup do
    @delivery_slot_name = delivery_slot_names(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:delivery_slot_names)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create delivery_slot_name" do
    assert_difference('DeliverySlotName.count') do
      post :create, delivery_slot_name: @delivery_slot_name.attributes
    end

    assert_redirected_to delivery_slot_name_path(assigns(:delivery_slot_name))
  end

  test "should show delivery_slot_name" do
    get :show, id: @delivery_slot_name.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @delivery_slot_name.to_param
    assert_response :success
  end

  test "should update delivery_slot_name" do
    put :update, id: @delivery_slot_name.to_param, delivery_slot_name: @delivery_slot_name.attributes
    assert_redirected_to delivery_slot_name_path(assigns(:delivery_slot_name))
  end

  test "should destroy delivery_slot_name" do
    assert_difference('DeliverySlotName.count', -1) do
      delete :destroy, id: @delivery_slot_name.to_param
    end

    assert_redirected_to delivery_slot_names_path
  end
end
