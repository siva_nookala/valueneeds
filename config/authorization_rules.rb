authorization do
  role :admin do
    includes :buyer
    has_permission_on [:products, :pincodes, :categories, :brands, :locations, :orders, :product_price_ranges, :wallet_entries, :promo_groups, :promos, :specialities, :scroll_texts, :system_settings, :notify_me_requests, :delivery_slot_names, :delivery_slots, :galleries,:recipes], :to=>[:index, :new, :create, :update, :destroy, :show, :edit]
    has_permission_on [:products], :to=>[:add_child_form, :update_category_child, :remove_category, :update_product_location_prices, :add_product_location_price_form, :remove_location_price, :request_products, :clean_orders]
    has_permission_on [:categories], :to => [:update_sequence_ids, :change_category_sequence_order]
    has_permission_on [:address_details], :to=>[:edit, :show, :update, :destroy]
    has_permission_on [:users], :to=>[:index, :edit, :update, :update_user_details]
    has_permission_on [:feedbacks], :to=>[:index,:update,:edit,:destroy]
    has_permission_on [:product_price_ranges], :to=>[:load_product_price_range_ids]
    has_permission_on [:promos], :to=>[:load_all_promo_ids]
    has_permission_on [:orders], :to=>[:status,:order_status_details, :payment_status]
    has_permission_on [:wallet_entries], :to=>[:show_wallet]
    has_permission_on [:delivery_slots], :to=>[:create_delivery_slots]
    has_permission_on [:bottom_banners], :to=>[:edit, :update, :destroy, :new,:index,:create]
  end

  role :buyer do
    has_permission_on [:address_details], :to=>[:index, :create, :create_order_with_previous_address, :get_address_details, :get_delivery_slots, :create_order_with_new_address, :confirm_with_wallet, :pay_with_wallet, :create_order_from_payment_gateway]
    has_permission_on [:product_price_ranges], :to=>[:get_product_price_range]
    has_permission_on [:notify_me_requests], :to=>[:create]
    has_permission_on [:wallet_entries], :to =>[:my_wallet]
    has_permission_on [:products], :to =>[:show_wish_list_products]
    has_permission_on [:galleries], :to=>[:index]
    has_permission_on [:recipes], :to=>[:recipes_description,:index]
  end

  role :guest do
    has_permission_on [:products], :to=>[:order_online, :add_items_to_cart, :remove_order_item, :show_cart, :login_screen, :remove_order, :update_location, :empty_cart, :get_products, :get_products_by_ids, :load_auto_complete_search_products, :clear_auto_complete_text, :get_main_filtered_products, :show_search_products, :product_details_tab, :product_request, :change_product_selection_quantity, :change_product_tab_selection_quantity,:edit_product, :load_more,:add_items_to_wish_list,:remove_wish_list_items]
    has_permission_on [:orders], :to=>[:repeat_order, :get_previous_orders,:invoice_status_details,:order_tracking]
    has_permission_on [:feedbacks], :to=>[:new, :create, :show]
    has_permission_on [:brands], :to=>[:index, :get_brands_by_ids]
    has_permission_on [:categories], :to=>[:index, :get_child_categories]
    has_permission_on [:locations], :to=>[:index]
    has_permission_on [:pincodes], :to=>[:users_index]
    has_permission_on [:address_details], :to=>[:check_out, :new]
    has_permission_on [:promos], :to=>[:promo_offers, :promo_complete_details, :get_promos_by_group, :get_promo_offer_details]
    has_permission_on [:notify_me_requests], :to=>[:new]
    has_permission_on [:users], :to => [:new, :create, :show]
    has_permission_on [:recipes], :to => [:index, :recipes_description]
    has_permission_on [:delivery_slot_names], :to => [:slots_for_mobile]
    has_permission_on [:galleries], :to=>[:index]
  end
end
