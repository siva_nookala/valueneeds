class CreateProductPromoMappings < ActiveRecord::Migration
  def change
    create_table :product_promo_mappings do |t|
      t.integer :product_id
      t.integer :promo_id
      t.string  :promo_title

      t.timestamps
    end
  end
end
