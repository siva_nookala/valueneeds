class AddPromoGroupIdToPromos < ActiveRecord::Migration
  def change
    add_column :promos, :promo_group_id, :integer
  end
end
