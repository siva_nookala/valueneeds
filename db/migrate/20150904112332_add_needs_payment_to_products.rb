class AddNeedsPaymentToProducts < ActiveRecord::Migration
  def change
    add_column :products, :needs_payment, :boolean, :default => false
  end
end
