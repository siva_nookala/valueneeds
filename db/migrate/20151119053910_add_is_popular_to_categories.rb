class AddIsPopularToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :is_popular, :boolean
  end
end
