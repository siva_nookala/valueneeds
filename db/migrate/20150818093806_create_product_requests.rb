class CreateProductRequests < ActiveRecord::Migration
  def change
    create_table :product_requests do |t|
      t.text :product_name
      t.string :customer_name
      t.string :email
      t.string :phone_number

      t.timestamps
    end
  end
end
