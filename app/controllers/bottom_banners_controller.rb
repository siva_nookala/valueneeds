class BottomBannersController < ApplicationController
  filter_resource_access
  # GET /bottom_banners
  # GET /bottom_banners.json
  def index
    @bottom_banners = BottomBanner.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @bottom_banners }
    end
  end

  # GET /bottom_banners/1
  # GET /bottom_banners/1.json
  def show
    @bottom_banner = BottomBanner.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @bottom_banner }
    end
  end

  # GET /bottom_banners/new
  # GET /bottom_banners/new.json
  def new
    @bottom_banner = BottomBanner.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @bottom_banner }
    end
  end

  # GET /bottom_banners/1/edit
  def edit
    @bottom_banner = BottomBanner.find(params[:id])
  end

  # POST /bottom_banners
  # POST /bottom_banners.json
  def create
    @bottom_banner = BottomBanner.new(params[:bottom_banner])
    update_banner
    respond_to do |format|
      if @bottom_banner.save
        format.html { redirect_to "/bottom_banners", notice: 'Bottom banner was successfully created.' }
        format.json { render json: @bottom_banner, status: :created, location: @bottom_banner }
      else
        format.html { render action: "new" }
        format.json { render json: @bottom_banner.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /bottom_banners/1
  # PUT /bottom_banners/1.json
  def update
    @bottom_banner = BottomBanner.find(params[:id])
    update_banner
    respond_to do |format|
      if @bottom_banner.save
        format.html { redirect_to "/bottom_banners", notice: 'Bottom banner was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @bottom_banner.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_banner
    @bottom_banner.url = params[:bottom_banner][:url]
    uploaded_io = params[:bottom_banner][:image]
    if uploaded_io
      file_path_name = UUIDTools::UUID.random_create.to_s.delete '-'
      just_filename = File.basename(uploaded_io.original_filename)
      file_path_name += just_filename.sub(/[^\w\.\-]/, '_')
      File.open(Rails.root.join('public', 'bottom_banner', file_path_name), 'w+b') do |file|
        file.write(uploaded_io.read)
      end
      @bottom_banner.image = file_path_name
    end
  end

  # DELETE /bottom_banners/1
  # DELETE /bottom_banners/1.json
  def destroy
    @bottom_banner = BottomBanner.find(params[:id])
    @bottom_banner.destroy

    respond_to do |format|
      format.html { redirect_to bottom_banners_url }
      format.json { head :ok }
    end
  end
end
