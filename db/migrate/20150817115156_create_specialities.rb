class CreateSpecialities < ActiveRecord::Migration
  def change
    create_table :specialities do |t|
      t.string :name
      t.integer :category_id
      t.string :image
      t.boolean :active

      t.timestamps
    end
  end
end
