class AddShowOnTopToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :show_on_top, :boolean, :default=>false
  end
end
