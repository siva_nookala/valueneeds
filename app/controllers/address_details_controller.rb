class AddressDetailsController < ApplicationController
  include ApplicationHelper::SessionConstants
  include PromosHelper
  include ApplicationHelper

  filter_resource_access
  filter_access_to :check_out, :attribute_check => false
  filter_access_to :create_order_with_previous_address, :attribute_check => false
  filter_access_to :get_address_details, :attribute_check => false
  filter_access_to :get_delivery_slots, :attribute_check => false
  filter_access_to :create_order_with_new_address, :attribute_check => false
  filter_access_to :pay_with_wallet, :attribute_check => false
  filter_access_to :confirm_with_wallet, :attribute_check => false
  filter_access_to :create_order_from_payment_gateway, :attribute_check => false

  # GET /address_details
  # GET /address_details.json
  def index
    @address_details = AddressDetail.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @address_details }
    end
  end

  # GET /address_details/1
  # GET /address_details/1.json
  def show
    @address_detail = AddressDetail.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @address_detail }
    end
  end

  # GET /address_details/new
  # GET /address_details/new.json
  def new
    @address_detail = AddressDetail.new
    if current_user.blank?
      @user_session = UserSession.new
      @user = User.new
    end
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @address_detail }
    end
  end

  # GET /address_details/1/edit
  def edit
    @address_detail = AddressDetail.find(params[:id])
  end

  # POST /address_details
  # POST /address_details.json
  def create
    @address_detail = AddressDetail.new(params[:address_detail])

    pincode = Pincode.find_by_code(@address_detail.pincode)

    is_valid_pincode = !pincode.blank? && pincode.active

    @address_detail.errors['pincode_error'] = 'Currently we do not deliver at this pin code' if !is_valid_pincode

    if !current_user.blank?
      @address_detail.buyer_id = current_user.id
    end
    respond_to do |format|
      if is_valid_pincode && @address_detail.save
        format.html { redirect_to '/address_details/check_out', :notice => 'New address is successfully added.' }
      else
        format.html { render action: 'new' }
        format.json { render json: @address_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /address_details/1
  # PUT /address_details/1.json
  def update
    @address_detail = AddressDetail.find(params[:id])

    respond_to do |format|
      if @address_detail.update_attributes(params[:address_detail])
        format.html { redirect_to @address_detail, notice: 'Address detail was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @address_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /address_details/1
  # DELETE /address_details/1.json
  def destroy
    @address_detail = AddressDetail.find(params[:id])
    @address_detail.destroy

    respond_to do |format|
      format.html { redirect_to address_details_url }
      format.json { head :ok }
    end
  end

  def check_out
    @address_details = AddressDetail.new
    @address_details = AddressDetail.find(:all, :conditions => ["buyer_id = ?", current_user.id], :order => "created_at desc", :limit => 3) if !current_user.blank?
    @delivery_slots = DeliverySlotName.get_active_slots
    @cart_items = !current_user.blank? ? CartItem.find_all_by_user_id(current_user.id) : CartItem.find_all_by_session_id(session[SESSION_ID])
    @needs_payment = needs_payment?(@cart_items)
    if @cart_items.blank?
      redirect_to '/products/show_cart'
    else
      if current_user.blank? || @address_details.size < 1
        session['REDIRECT_URL'] = '/address_details/check_out'
        redirect_to '/address_details/new'
      end
    end
  end

  def create_order_with_previous_address
    redirect_url = ''
    valid_coupon = is_valid_coupon(params[:coupon_code], 'VN001')
    if params[:coupon_code].blank? || valid_coupon
      coupon_code = params[:coupon_code] if valid_coupon
      @cart_items = CartItem.find_all_by_user_id(current_user.id)
      if @cart_items.blank?
        redirect_url='/products/show_cart'
        notice = ''
      else
        total_price = get_total_charges_with_out_delivery_charges
        if params[:commit].eql?('Apply Coupon')
            coupon_discount = get_discount_on_coupon(total_price)
            redirect_url="/address_details/check_out?coupon_code=#{params[:coupon_code]}"
        elsif params[:commit].eql?('Pay Online')
          @online_payment = OnlinePayment.new
          random_id = UUIDTools::UUID.random_create.to_s.delete '-'
          random_id[0..2]=''
          @online_payment.merchant_transaction_id = random_id
          if !coupon_code.blank?
            @online_payment.coupon_code=coupon_code

            @online_payment.coupon_discount = get_discount_on_coupon(total_price)
            grand_total=total_price-@online_payment.coupon_discount
            delivery_charges = get_delivery_charges(total_price)
            @online_payment.transaction_amount = (grand_total+delivery_charges).round(2)
          else
            @online_payment.transaction_amount = get_total_amount
          end
          @online_payment.address_id = params[:address_id]
          @online_payment.payment_status = 'Created'
          @online_payment.save
          @cart_items.each do |cart_item|
            cart_item.update_attribute(:merchant_transaction_id, @online_payment.merchant_transaction_id);
          end
          redirect_url="/address_details/pay_with_wallet?mti=#{@online_payment.merchant_transaction_id}"
        else
          @needs_payment = needs_payment?(@cart_items)
          if !@needs_payment
            @address_detail = AddressDetail.find_by_id(params[:address_id])
            payment_mode = 'Cash'
            payment_mode = 'SodexHo_TicketRestaurant' if params[:commit].eql?('Pay With Sodexo/Ticket Restaurant')
            create_order(payment_mode, coupon_code)
            redirect_url="/orders/invoice_status_details?id=#{@order.id}"
            notice = 'Your Order Is Taken We will Get Back Soon'
          else
            redirect_url='/products/show_cart'
            notice = 'Can not use Cash On Delivery option for this.'
          end
        end

      end
    else
      error= !params[:coupon_code].eql?('VN001') ? 'Invalid-Coupon-Code.' : 'This-Coupon-Code-is-valid-only-for-the-first-order.'
      redirect_url="/address_details/check_out?coupon_error=#{error}"
    end
    respond_to do |format|
      format.html {  redirect_to redirect_url, :notice => notice}
      format.json { render json: {:status => notice, :order_id => !@order.blank? ? @order.id : -1} }
    end
  end

  def create_order_from_payment_gateway

    is_online_payment_complete = false

    reason = 'Payment Gateway Failed'

    if !params[:mti].blank?
      @online_payment = OnlinePayment.find_by_merchant_transaction_id(params[:mti])
      if !@online_payment.blank? && @online_payment.user_id == current_user.id && @online_payment.payment_status.eql?('Success')
        CartItem.where('user_id = ? and merchant_transaction_id != ?', current_user.id, @online_payment.merchant_transaction_id).destroy_all
        @total_amount = get_total_amount
        if @online_payment.transaction_amount >= @total_amount
          @address_detail = AddressDetail.find_by_id(@online_payment.address_id)
          is_online_payment_complete = true
        else
          reason = 'Amounts do not match ' + @online_payment.transaction_amount.to_s + ' is less than ' + @total_amount
        end
      else
        reason = 'Invalid payment status ' + @online_payment.payment_status
      end
    else
      reason = 'Invalid Transcation Id'
    end
    if is_online_payment_complete
      create_order('Paid_Online', @online_payment.coupon_code)
      @online_payment.update_attribute(:order_id, @order.id)
      redirect_to "/orders/invoice_status_details?id=#{@order.id}", :notice => 'Your Order Is Taken We will Get Back Soon'
    else
      redirect_to "/products/show_cart", :notice => 'Sorry. Your payment could not be completed. Reason: ' + reason
    end

  end

  def pay_with_wallet
    @online_payment = OnlinePayment.find_by_merchant_transaction_id(params[:mti])
    @wallet_balance = get_wallet_balance
    @total_amount = get_total_amount
    @pending_amount = (@online_payment.transaction_amount - @wallet_balance).round(2)
    @pending_amount = 0 if @pending_amount <= 0
  end

  def confirm_with_wallet
    @wallet_balance = get_wallet_balance
    @total_amount = get_total_amount
    @pending_amount = @total_amount - @wallet_balance
    @pending_amount = 0 if @pending_amount <= 0
    @online_payment = OnlinePayment.find_by_merchant_transaction_id(params[:mti])

    if @pending_amount <= 0
      @wallet_entry = WalletEntry.new
      @wallet_entry.user_id = current_user.id
      @wallet_entry.amount = -@total_amount
      @wallet_entry.reason = 'Paid for order'
      @wallet_entry.transaction_id = @online_payment.merchant_transaction_id
      @wallet_entry.save!
      @online_payment.wallet_amount = @total_amount
      @online_payment.gateway_amount = 0.0
      @online_payment.payment_status = 'Paid'
      @online_payment.user_id = current_user.id
      @online_payment.save!
      redirect_to "/address_details/create_order_from_payment_gateway?mti=#{params[:mti]}"
    else
      @online_payment.wallet_amount = @wallet_balance
      @online_payment.gateway_amount = @pending_amount
      @online_payment.payment_status = 'Pending'
      @online_payment.user_id = current_user.id
      @online_payment.save!
      payment_url = cc_avenue_details(@online_payment, "#{root_url}/static/payment_response_handler")
      redirect_to payment_url
    end

  end

  def get_address_details
    @address_details = AddressDetail.new
    @address_details = AddressDetail.find(:all, :conditions => ["buyer_id = ?", current_user.id], :order => "created_at desc", :limit => 3) if !current_user.blank?
    respond_to do |format|
      format.json { render json: @address_details }
    end
  end

  def get_delivery_slots
    @delivery_slots = DeliverySlotName.get_active_slots
    respond_to do |format|
      format.json { render json: @delivery_slots }
    end
  end

  def create_order(payment_mode, coupon_code='')
    @order = Order.new
    @order.buyer_id = current_user.id if !current_user.blank?
    @order.address_id = @address_detail.id
    deliverySlotName = DeliverySlotName.find_by_id(params[:delivery_slot])
    @order.delivery_slot_name = deliverySlotName.blank? ? params[:chosen_delivery_time] : deliverySlotName.slot_name
    @order.status = 'Created'
    @order.payment_status = 'Pending'
    @order.coupon_code = coupon_code
      if request.format=='json'
        @order.order_by='Mobile'
      else
        @order.order_by='Web'
      end
    @order.location_id = !session[ApplicationHelper::SessionConstants::BUYER_LOCATION].blank? ? session[ApplicationHelper::SessionConstants::BUYER_LOCATION] : Location.first.id
    @order.save
    total_price = 0.0
    @total_cart_items = !current_user.blank? ? CartItem.find_all_by_user_id(current_user.id) : CartItem.find_all_by_session_id(session[SESSION_ID])
    category_lists = get_cart_by_category_level(@total_cart_items)
    category_lists.each do |category_list|
      cart_items = category_list.cart_order_items
      promo_range = category_list.promo
      cart_items.each_with_index do |item, index|
        order_item = OrderItem.new
        order_item.product_id = item.order_item.product_id
        order_item.quantity = item.order_item.quantity
        order_item.price =item.order_item.original_price.to_d
        order_item.product_attributes = item.order_item.product_attributes if !item.order_item.product_attributes.blank?
        order_item.effective_price = item.order_item.effective_price.to_d
        order_item.total_price = item.order_item.sub_total
        order_item.savings_amount = (order_item.price - order_item.effective_price)*order_item.quantity.to_i
        order_item.order_id = @order.id
        if !item.promo.blank?
          order_item.promo_id = item.promo.promo_id
          order_item.promo_quantity = item.promo.offer_limit_quantity
          order_item.promo_offer_type = item.promo.offer_type
          order_item.savings_amount = item.promo.savings_amount
        end
        if !promo_range.blank? && index+1 == cart_items.size
          order_item.promo_id = promo_range.promo_id
          order_item.promo_quantity = promo_range.offer_limit_quantity
          order_item.promo_offer_type = promo_range.offer_type
          order_item.savings_amount = promo_range.savings_amount
        end
        order_item.save!
        total_price = total_price + order_item.total_price.to_d.round(2)
      end
    end
    @order.delivery_charges = get_delivery_charges(total_price)
    if !@order.coupon_code.blank?
      @order.coupon_discount=get_discount_on_coupon(total_price)
      total_price = total_price-@order.coupon_discount
    end
    @order.total_amount = (total_price + @order.delivery_charges).round(2)
    @order.save!
    @order.tracking_id = 'VN' + (3000+@order.id).to_s
    @order.save!
    @total_cart_items.each do |cart_item|
      cart_item.destroy
    end
    if @order.save
      @order.update_attribute(:payment_mode, payment_mode)
      OrderMailer.send_status_mail(current_user, @order, SystemSetting.find_by_key('copy_emails').value).deliver
      send_sms(@address_detail.contact_number, "Dear #{current_user.display_name},%0AThank you for placing your order for Rs. #{@order.total_amount} at ValueNeeds. Your order id is #{@order.tracking_id}. We will deliver the order at #{@order.delivery_slot_name}.")
      order_alert_numbers = SystemSetting.find_by_key('order_alert_numbers').value if !SystemSetting.find_by_key('order_alert_numbers').blank?
      if !order_alert_numbers.blank?
        order_alert_numbers.split(',').each do |mobile_number|
          send_sms(mobile_number, "#{current_user.display_name}(#{@address_detail.contact_number})%0A#{@address_detail.house_no} #{@address_detail.area} #{@address_detail.land_mark} #{@address_detail.city} #{@address_detail.pincode}%0A#{@order.tracking_id}%0A#{@order.delivery_slot_name}%0ARs. #{@order.total_amount}%0Acash")
        end
      end
    end
  end

  def get_wallet_balance
    wallet_entries = WalletEntry.find_all_by_user_id(current_user.id)
    result = 0
    wallet_entries.each do |wallet_entry|
      result += wallet_entry.amount
    end
    result
  end



  def  discount

  end


end
