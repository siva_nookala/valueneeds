class Product < ActiveRecord::Base

  validates_presence_of :name, :brand_id, :picture, :price,  :quantity
  scope :active, -> { where("active = 't' or active = '1'")}

  def self.category_scope(category_ids)
    if category_ids.blank?
      scoped
    else
      self.where('id in (select product_id from product_categories where category_id in (?))', category_ids)
    end
  end

  def self.brand_scope(brand_ids)
    if brand_ids.blank?
      scoped
    else
      self.where('brand_id in (?)', brand_ids)
    end
  end

  def self.price_scope(price_range_ids)
    if price_range_ids.blank?
      scoped
    else
      self.where('product_price_range_id in (?)', price_range_ids)
    end
  end

  def self.sort_scope(sort_key)
    if sort_key.blank?
      self.order('updated_at DESC')
    elsif sort_key.eql?('PRICE_HIGH_TO_LOW')
      self.order('price DESC')
    elsif sort_key.eql?('PRICE_LOW_TO_HIGH')
      self.order('price ASC')
    elsif sort_key.eql?('NAME_ASCENDING')
      self.order('name ASC')
    end
  end

  def self.search_scope(key)
     if key.blank?
       scoped
     else
     self.where("name like ? OR id in (select product_id from product_categories where category_id in (select id from categories where parent_id in(select id from categories where name like ?)))", "%#{key}%", "%#{key}%")
     end
  end

end
