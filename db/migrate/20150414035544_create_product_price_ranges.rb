class CreateProductPriceRanges < ActiveRecord::Migration
  def change
    create_table :product_price_ranges do |t|
      t.decimal :start_price
      t.decimal :end_price
      t.string :description

      t.timestamps
    end
  end
end
