class RecipesController < ApplicationController
  filter_resource_access
  filter_access_to :recipes_description, :attribute_check => false
  include ApplicationHelper
  def index
    @recipes = Recipe.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @recipes }
    end
  end

  # GET /recipes/1
  # GET /recipes/1.json
  def show
    @recipe = Recipe.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @recipe }
    end
  end

  # GET /recipes/new
  # GET /recipes/new.json
  def new
    @recipe = Recipe.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @recipe }
    end
  end

  # GET /recipes/1/edit
  def edit
    @recipe = Recipe.find(params[:id])
  end

  # POST /recipes
  # POST /recipes.json
  def create
    @recipe = Recipe.new(params[:recipe])
    update_recipies_details
    respond_to do |format|
      if @recipe.save
        format.html { redirect_to @recipe, notice: 'Recipe was successfully created.' }
        format.json { render json: @recipe, status: :created, location: @recipe }
      else
        format.html { render action: "new" }
        format.json { render json: @recipe.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /recipes/1
  # PUT /recipes/1.json
  def update
    @recipe = Recipe.find(params[:id])
    update_recipies_details
    respond_to do |format|
      if @recipe.update_attributes(params[:recipe])
        format.html { redirect_to @recipe, notice: 'Recipe was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @recipe.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_recipies_details
    @recipe.category_id = params[:category_id]
    @recipe.title = params[:recipe][:title]
    @recipe.procedure = params[:recipe][:procedure]
    @recipe.ingredients = params[:recipe][:ingredients]
    uploaded_io = params[:recipe][:small_picture]
    if uploaded_io
      file_path_name = SecureRandom.uuid.delete('-')
      just_filename = File.basename(uploaded_io.original_filename)
      file_path_name += just_filename.sub(/[^\w\.\-]/, '_')
      File.open(Rails.root.join('public', 'recipes', file_path_name), 'w+b') do |file|
        file.write(uploaded_io.read)
      end
      @recipe.small_picture = file_path_name
    end
    uploaded_io = params[:recipe][:large_picture]
    if uploaded_io
      file_path_name = SecureRandom.uuid.delete('-')
      just_filename = File.basename(uploaded_io.original_filename)
      file_path_name += just_filename.sub(/[^\w\.\-]/, '_')
      File.open(Rails.root.join('public', 'recipes', file_path_name), 'w+b') do |file|
        file.write(uploaded_io.read)
      end
      @recipe.large_picture = file_path_name
    end
  end

  # DELETE /recipes/1
  # DELETE /recipes/1.json
  def destroy
    @recipe = Recipe.find(params[:id])
    @recipe.destroy

    respond_to do |format|
      format.html { redirect_to recipes_url }
      format.json { head :ok }
    end
  end

  def recipes_description
    @recipe = Recipe.find_by_id(params[:recipe_id])
  end
end
