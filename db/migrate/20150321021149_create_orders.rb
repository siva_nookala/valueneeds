class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :buyer_id
      t.string :payment_status
      t.string :status
      t.string :total_amount
      t.string :payment_mode
      t.integer :parent_id
      t.integer :address_id

      t.timestamps
    end
  end
end
