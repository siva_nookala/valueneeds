class OrdersController < ApplicationController
  filter_resource_access
  filter_access_to :get_previous_orders, :attribute_check => false
  filter_access_to :repeat_order, :attribute_check => false
  filter_access_to :invoice_status_details, :attribute_check => false
  filter_access_to :order_tracking, :attribute_check => false
  filter_access_to :status, :attribute_check => false
  filter_access_to :order_status_details, :attribute_check => false

  include ApplicationHelper::SessionConstants
  include ProductsHelper
  include ApplicationHelper
  include OrdersHelper
  # GET /orders
  # GET /orders.json
  def index
    @order_filter = OrdersFilter.new
    @order_filter.search_date = params[:search_date]
    @order_filter.start_date = params[:start_date]
    @order_filter.end_date = params[:end_date]
    @order_filter.order_id = params[:order_id]
    @order_filter.location_id = params[:location_id]
    @order_filter.order_status = params[:order_status]
    @order_filter.payment_status = params[:payment_status]
    effective_start_date = nil
    effective_end_date = Time.now.midnight + 1.day
    if @order_filter.search_date == 'To Day'
      effective_start_date = Time.now
    elsif @order_filter.search_date == 'Last Two Days'
      effective_start_date = Time.now - 1.days
    elsif @order_filter.search_date == 'Last Week'
      effective_start_date = Time.now - 7.days
    elsif @order_filter.search_date == 'Last Month'
      effective_start_date = Time.now - 30.days
    elsif @order_filter.search_date == 'Date Range'
      if !@order_filter.start_date.blank?
        effective_start_date = Date.parse(@order_filter.start_date).midnight
      end
      if !@order_filter.end_date.blank?
        effective_end_date = Date.parse(@order_filter.end_date).midnight + 1.day
      end
    end
    effective_start_date = effective_start_date.midnight unless effective_start_date.nil?
    where_conditions = {}
    @order_filter.start_date = effective_start_date.to_date unless effective_start_date.nil?
    @order_filter.end_date = (effective_end_date - 1.day).to_date
    if !@order_filter.location_id.blank? && !@order_filter.location_id.include?('-1')
      where_conditions.store(:location_id, @order_filter.location_id)
    end

    if !@order_filter.order_id.blank?
      where_conditions.store(:tracking_id, @order_filter.order_id)
    end

    if !@order_filter.payment_status.blank? && !@order_filter.payment_status.include?('Show All')
      where_conditions.store(:payment_status, @order_filter.payment_status)
    end
    if !@order_filter.order_status.blank? && !@order_filter.order_status.include?('Show All')
      where_conditions.store(:status, @order_filter.order_status)
    end
    if !effective_start_date.nil?
      @orders = Order.where(where_conditions).where('created_at between ? and ? ', effective_start_date, effective_end_date).reverse_order
    else
      @orders = Order.where(where_conditions).where('created_at <= ? ', effective_end_date).reverse_order
    end
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
    @order = Order.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @order }
    end
  end

  # GET /orders/new
  # GET /orders/new.json
  def new
    @order = Order.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @order }
    end
  end

  # GET /orders/1/edit
  def edit
    @order = Order.find(params[:id])
  end

  # POST /orders
  # POST /orders.json
  def create
    @order = Order.new(params[:order])

    respond_to do |format|
      if @order.save
        format.html { redirect_to @order, notice: 'Order was successfully created.' }
        format.json { render json: @order, status: :created, location: @order }
      else
        format.html { render action: "new" }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /orders/1
  # PUT /orders/1.json
  def update
    @order = Order.find(params[:id])

    respond_to do |format|
      if @order.update_attributes(params[:order])
        format.html { redirect_to @order, notice: 'Order was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    @order = Order.find(params[:id])
    @order.destroy

    respond_to do |format|
      format.html { redirect_to orders_url }
      format.json { head :ok }
    end
  end

  def get_previous_orders
    if !current_user.blank?
      @orders = Order.find_all_by_buyer_id(current_user.id)
    end
    respond_to do |format|
      format.html
      format.json { render json: @orders }
    end
  end

  def invoice_status_details
    @order = Order.find(params[:id])
    @address_details = AddressDetail.find(@order.address_id)
    @delivery_time = @order.delivery_slot_name
    @order_items = OrderItem.find_all_by_order_id(@order.id)

    if request.format.json?
      @order_items.each do |order_item|
        order_item.product_name = Product.find_by_id(order_item.product_id).try(:name)
      end
    end

    if params[:print].blank?
      respond_to do |format|
        format.html
        format.json { render :json => {:order => @order,
                                       :address_details => @address_details, :delivery_time => @delivery_time, :order_items => @order_items} }
      end
    else
      respond_to do |format|
        format.html { render :layout => false }
        format.json { render :json => {:order => @order,
                                       :address_details => @address_details, :delivery_time => @delivery_time, :order_items => @order_items} }
      end
    end
  end

  def order_tracking

  end

  def status
    order_status_history = OrderStatusHistory.new
    @order = Order.find(params[:id])
    order_status_history.order_id = @order.id
    order_status_history.user_id = current_user.id
    order_status_history.status_type = 'Status'
    order_status_history.old_status = @order.status
    @order.status = params[:status]
    @order.save
    order_status_history.new_status = @order.status
    order_status_history.save


    respond_to do |format|
      format.js
    end
  end

  def payment_status
    order_status_history = OrderStatusHistory.new
    @order = Order.find(params[:id])
    order_status_history.order_id = @order.id
    order_status_history.user_id = current_user.id
    order_status_history.status_type = 'PaymentStatus'
    order_status_history.old_status = @order.payment_status
    @order.payment_status = params[:payment_status]
    @order.save
    order_status_history.new_status = @order.payment_status
    order_status_history.save
    respond_to do |format|
      format.js
    end
  end

  def order_status_details
    search_id = params[:id]
    search_id = search_id.strip if !search_id.blank?
    @order = Order.find_by_id_and_buyer_id(search_id, current_user.id)
    @order = Order.find_by_tracking_id_and_buyer_id(search_id, current_user.id) if @order.blank?
    respond_to do |format|
      format.js
    end
  end

  def order_track_detail

  end


  def repeat_order
    if !params[:order_id].blank?
      @order_items = OrderItem.find_all_by_order_id(params[:order_id])
      session[SESSION_ORDER_ITEMS] = []
      @order_items.each do |order_item|
        if !order_item.blank?
          session_order_item = SessionOrderItem.new
          product = Product.find_by_id(order_item.product_id)
          session_order_item.item_id = product.id
          session_order_item.quantity = order_item.quantity
          session_order_item.price = product.price
          session[SESSION_ORDER_ITEMS] << session_order_item
        end
      end
    end
    redirect_to '/address_details/new'
  end

end
