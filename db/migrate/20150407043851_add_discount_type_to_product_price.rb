class AddDiscountTypeToProductPrice < ActiveRecord::Migration
  def change
    add_column :product_prices, :discount_type, :string
  end
end
