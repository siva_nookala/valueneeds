class DeliverySlotNamesController < ApplicationController
  filter_resource_access
  # GET /delivery_slot_names
  # GET /delivery_slot_names.json
  filter_access_to :slots_for_mobile, :attribute_check => false
  def index
    @delivery_slot_names = DeliverySlotName.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @delivery_slot_names }
    end
  end

  # GET /delivery_slot_names/1
  # GET /delivery_slot_names/1.json
  def show
    @delivery_slot_name = DeliverySlotName.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @delivery_slot_name }
    end
  end

  # GET /delivery_slot_names/new
  # GET /delivery_slot_names/new.json
  def new
    @delivery_slot_name = DeliverySlotName.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @delivery_slot_name }
    end
  end

  # GET /delivery_slot_names/1/edit
  def edit
    @delivery_slot_name = DeliverySlotName.find(params[:id])
  end

  # POST /delivery_slot_names
  # POST /delivery_slot_names.json
  def create
    @delivery_slot_name = DeliverySlotName.new(params[:delivery_slot_name])

    respond_to do |format|
      if @delivery_slot_name.save
        format.html { redirect_to @delivery_slot_name, notice: 'Delivery slot name was successfully created.' }
        format.json { render json: @delivery_slot_name, status: :created, location: @delivery_slot_name }
      else
        format.html { render action: "new" }
        format.json { render json: @delivery_slot_name.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /delivery_slot_names/1
  # PUT /delivery_slot_names/1.json
  def update
    @delivery_slot_name = DeliverySlotName.find(params[:id])

    respond_to do |format|
      if @delivery_slot_name.update_attributes(params[:delivery_slot_name])
        format.html { redirect_to @delivery_slot_name, notice: 'Delivery slot name was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @delivery_slot_name.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /delivery_slot_names/1
  # DELETE /delivery_slot_names/1.json
  def destroy
    @delivery_slot_name = DeliverySlotName.find(params[:id])
    @delivery_slot_name.destroy

    respond_to do |format|
      format.html { redirect_to delivery_slot_names_url }
      format.json { head :ok }
    end
  end

  def slots_for_mobile

  end
end
