class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :user_name
      t.string :email
      t.string :phone
      t.string :display_name
      t.string :persistence_token
      t.string :password_salt
      t.string :crypted_password
      t.boolean :admin

      t.timestamps
    end
  end
end
