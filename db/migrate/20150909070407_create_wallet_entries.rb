class CreateWalletEntries < ActiveRecord::Migration
  def change
    create_table :wallet_entries do |t|
      t.integer :user_id
      t.decimal :amount
      t.string :reason
      t.integer :order_id
      t.integer :transaction_id

      t.timestamps
    end
  end
end
