class AddProductAttributesToOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :product_attributes, :string
  end
end
