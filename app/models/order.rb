class Order < ActiveRecord::Base

  def get_products_total
    total_amount.to_f - delivery_charges
  end
end
