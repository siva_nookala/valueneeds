require 'test_helper'

class ProductPriceRangesControllerTest < ActionController::TestCase
  setup do
    @product_price_range = product_price_ranges(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:product_price_ranges)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create product_price_range" do
    assert_difference('ProductPriceRange.count') do
      post :create, product_price_range: @product_price_range.attributes
    end

    assert_redirected_to product_price_range_path(assigns(:product_price_range))
  end

  test "should show product_price_range" do
    get :show, id: @product_price_range.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @product_price_range.to_param
    assert_response :success
  end

  test "should update product_price_range" do
    put :update, id: @product_price_range.to_param, product_price_range: @product_price_range.attributes
    assert_redirected_to product_price_range_path(assigns(:product_price_range))
  end

  test "should destroy product_price_range" do
    assert_difference('ProductPriceRange.count', -1) do
      delete :destroy, id: @product_price_range.to_param
    end

    assert_redirected_to product_price_ranges_path
  end
end
