class SpecialitiesController < ApplicationController
  filter_resource_access
  # GET /specialities
  # GET /specialities.json
  def index
    @specialities = Speciality.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @specialities }
    end
  end

  # GET /specialities/1
  # GET /specialities/1.json
  def show
    @speciality = Speciality.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @speciality }
    end
  end

  # GET /specialities/new
  # GET /specialities/new.json
  def new
    @speciality = Speciality.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @speciality }
    end
  end

  # GET /specialities/1/edit
  def edit
    @speciality = Speciality.find(params[:id])
  end

  # POST /specialities
  # POST /specialities.json
  def create
    @speciality = Speciality.new
    update_speciality
    respond_to do |format|
      if @speciality.save
        format.html { redirect_to @speciality, notice: 'Speciality was successfully created.' }
        format.json { render json: @speciality, status: :created, location: @speciality }
      else
        format.html { render action: "new" }
        format.json { render json: @speciality.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /specialities/1
  # PUT /specialities/1.json
  def update
    @speciality = Speciality.find(params[:id])
    update_speciality
    respond_to do |format|
      if @speciality.save
        format.html { redirect_to @speciality, notice: 'Speciality was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @speciality.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /specialities/1
  # DELETE /specialities/1.json
  def destroy
    @speciality = Speciality.find(params[:id])
    @speciality.destroy

    respond_to do |format|
      format.html { redirect_to specialities_url }
      format.json { head :ok }
    end
  end

  def update_speciality
    @speciality.name = params[:speciality][:name]
    @speciality.category_id = params[:speciality][:category_id]
    @speciality.active = params[:speciality][:active]
    @speciality.order_id = params[:speciality][:order_id]
    uploaded_io = params[:speciality][:image]
    if uploaded_io
      file_path_name = UUIDTools::UUID.random_create.to_s.delete '-'
      just_filename = File.basename(uploaded_io.original_filename)
      file_path_name += just_filename.sub(/[^\w\.\-]/, '_')
      File.open(Rails.root.join('public', 'speciality', file_path_name), 'w+b') do |file|
        file.write(uploaded_io.read)
      end
      @speciality.image = file_path_name
    end
  end
end
