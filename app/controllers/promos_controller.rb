class PromosController < ApplicationController
  include PromosHelper
  filter_resource_access
  filter_access_to :load_all_promo_ids, :attribute_check => false
  filter_access_to :get_promos_by_group, :attribute_check => false
  filter_access_to :promo_offers, :attribute_check => false
  filter_access_to :promo_complete_details, :attribute_check => false
  filter_access_to :get_promo_offer_details, :attribute_check => false
  # GET /promos
  # GET /promos.json
  def index
    @promos = Promo.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @promos }
    end
  end

  # GET /promos/1
  # GET /promos/1.json
  def show
    @promo = Promo.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @promo }
    end
  end

  # GET /promos/new
  # GET /promos/new.json
  def new
    @promo = Promo.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @promo }
    end
  end

  # GET /promos/1/edit
  def edit
    @promo = Promo.find(params[:id])
  end

  # POST /promos
  # POST /promos.json
  def create
    @promo = Promo.new(params[:promo])

    respond_to do |format|
      if @promo.save
        format.html { redirect_to @promo, notice: 'Promo was successfully created.' }
        format.json { render json: @promo, status: :created, location: @promo }
      else
        format.html { render action: "new" }
        format.json { render json: @promo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /promos/1
  # PUT /promos/1.json
  def update
    @promo = Promo.find(params[:id])

    respond_to do |format|
      if @promo.update_attributes(params[:promo])
        format.html { redirect_to @promo, notice: 'Promo was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @promo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /promos/1
  # DELETE /promos/1.json
  def destroy
    @promo = Promo.find(params[:id])
    @promo.destroy

    respond_to do |format|
      format.html { redirect_to promos_url }
      format.json { head :ok }
    end
  end

  def load_all_promo_ids
   promos = Promo.all
   promos.each do |promo|
     if !promo.blank?
     promo_product_map = ProductPromoMapping.find_by_promo_id(promo.id)
       if promo_product_map.blank?
         if !promo.product_id.blank?
           product_promo_map = ProductPromoMapping.new
           product_promo_map.promo_id = promo.id
           product_promo_map.product_id = promo.product_id
           product_promo_map.promo_title = promo.promo_title
           product_promo_map.save!
         end
         if !promo.product_range_id.blank?
           product_categories = ProductCategory.find_all_by_category_id(promo.product_range_id)
           product_categories.each do |product_category|
             product_promo_map = ProductPromoMapping.new
             product_promo_map.promo_id = promo.id
             product_promo_map.promo_title = promo.promo_title
             product_promo_map.product_id = product_category.product_id
             product_promo_map.save!
           end
         end
       else
         if !promo.product_id.blank?
           promo_product_map.promo_id = promo.id
           promo_product_map.product_id = promo.product_id
           promo_product_map.promo_title = promo.promo_title
           promo_product_map.save!
         end
      end
     end
    end
   redirect_to '/promos'
  end

  def get_promos_by_group
    @promo_groups = Promo.find_all_by_promo_group_id(params[:promo_group_id])
    respond_to do |format|
      format.json { render json: @promo_groups.collect{|group| group.id}}
    end
  end

  def promo_offers
    @promos = Promo.find(:all, :conditions => ['valid_from <= ? and valid_till >= ?', Time.now, Time.now])
    respond_to do |format|
      format.html
      format.json { render json: {:promos => @promos.collect { |promo| [promo.id , promo.promo_title] }} }
    end
  end

  def promo_complete_details
    @current_promo = Promo.find(params[:promo_id])
  end

  def get_promo_offer_details
    promo = Promo.find(params[:promo_id])
    respond_to do |format|
      format.json { render json: {:promo_offer_details => get_promo_details(promo), :promo_basket_details => get_promo_products_details(promo)} }
    end
  end
end
