class CreatePincodes < ActiveRecord::Migration
  def change
    create_table :pincodes do |t|
      t.string :code
      t.boolean :active

      t.timestamps
    end
  end
end
