require 'test_helper'

class BottomBannersControllerTest < ActionController::TestCase
  setup do
    @bottom_banner = bottom_banners(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:bottom_banners)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create bottom_banner" do
    assert_difference('BottomBanner.count') do
      post :create, bottom_banner: @bottom_banner.attributes
    end

    assert_redirected_to bottom_banner_path(assigns(:bottom_banner))
  end

  test "should show bottom_banner" do
    get :show, id: @bottom_banner.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @bottom_banner.to_param
    assert_response :success
  end

  test "should update bottom_banner" do
    put :update, id: @bottom_banner.to_param, bottom_banner: @bottom_banner.attributes
    assert_redirected_to bottom_banner_path(assigns(:bottom_banner))
  end

  test "should destroy bottom_banner" do
    assert_difference('BottomBanner.count', -1) do
      delete :destroy, id: @bottom_banner.to_param
    end

    assert_redirected_to bottom_banners_path
  end
end
