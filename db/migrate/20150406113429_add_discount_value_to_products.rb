class AddDiscountValueToProducts < ActiveRecord::Migration
  def change
    add_column :products, :discount_value, :string
  end
end
