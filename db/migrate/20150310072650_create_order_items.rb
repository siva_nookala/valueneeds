class CreateOrderItems < ActiveRecord::Migration
  def change
    create_table :order_items do |t|
      t.integer :order_id
      t.integer :product_id
      t.string :price
      t.string :discount_price
      t.string :quantity
      t.string :total_price

      t.timestamps
    end
  end
end
