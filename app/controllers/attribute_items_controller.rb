class AttributeItemsController < ApplicationController
  # GET /attribute_items
  # GET /attribute_items.json
  def index
    @attribute_items = AttributeItem.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @attribute_items }
    end
  end

  # GET /attribute_items/1
  # GET /attribute_items/1.json
  def show
    @attribute_item = AttributeItem.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @attribute_item }
    end
  end

  # GET /attribute_items/new
  # GET /attribute_items/new.json
  def new
    @attribute_item = AttributeItem.new
    @attribute_item.attribute_id = params[:id]
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @attribute_item }
    end
  end

  # GET /attribute_items/1/edit
  def edit
    @attribute_item = AttributeItem.find(params[:id])
  end

  # POST /attribute_items
  # POST /attribute_items.json
  def create
    @attribute_item = AttributeItem.new(params[:attribute_item])

    respond_to do |format|
      if @attribute_item.save
        format.html { redirect_to '/attributes', notice: 'Attribute item was successfully created.' }
        format.json { render json: @attribute_item, status: :created, location: @attribute_item }
      else
        format.html { render action: "new" }
        format.json { render json: @attribute_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /attribute_items/1
  # PUT /attribute_items/1.json
  def update
    @attribute_item = AttributeItem.find(params[:id])

    respond_to do |format|
      if @attribute_item.update_attributes(params[:attribute_item])
        format.html { redirect_to @attribute_item, notice: 'Attribute item was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @attribute_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /attribute_items/1
  # DELETE /attribute_items/1.json
  def destroy
    @attribute_item = AttributeItem.find(params[:id])
    @attribute_item.destroy

    respond_to do |format|
      format.html { redirect_to attribute_items_url }
      format.json { head :ok }
    end
  end
end
