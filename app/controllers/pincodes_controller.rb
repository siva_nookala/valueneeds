class PincodesController < ApplicationController
  filter_resource_access
  # GET /pincodes
  # GET /pincodes.json
  filter_access_to :users_index, :attribute_check => false

  def index
    @pincodes = Pincode.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @pincodes }
    end
  end

  def users_index
    @pincodes = Pincode.find_all_by_active(true)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @pincodes }
    end
  end

  # GET /pincodes/1
  # GET /pincodes/1.json
  def show
    @pincode = Pincode.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @pincode }
    end
  end

  # GET /pincodes/new
  # GET /pincodes/new.json
  def new
    @pincode = Pincode.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @pincode }
    end
  end

  # GET /pincodes/1/edit
  def edit
    @pincode = Pincode.find(params[:id])
  end

  # POST /pincodes
  # POST /pincodes.json
  def create
    @pincode = Pincode.new(params[:pincode])

    respond_to do |format|
      if @pincode.save
        format.html { redirect_to @pincode, notice: 'Pincode was successfully created.' }
        format.json { render json: @pincode, status: :created, location: @pincode }
      else
        format.html { render action: "new" }
        format.json { render json: @pincode.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /pincodes/1
  # PUT /pincodes/1.json
  def update
    @pincode = Pincode.find(params[:id])

    respond_to do |format|
      if @pincode.update_attributes(params[:pincode])
        format.html { redirect_to @pincode, notice: 'Pincode was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @pincode.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pincodes/1
  # DELETE /pincodes/1.json
  def destroy
    @pincode = Pincode.find(params[:id])
    @pincode.destroy

    respond_to do |format|
      format.html { redirect_to pincodes_url }
      format.json { head :ok }
    end
  end
end
