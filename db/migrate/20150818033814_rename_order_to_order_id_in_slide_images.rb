class RenameOrderToOrderIdInSlideImages < ActiveRecord::Migration
  def change
      rename_column :slide_images, :order, :order_id
      rename_column :slide_images, :category_name, :name
  end
end
