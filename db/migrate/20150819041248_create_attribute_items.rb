class CreateAttributeItems < ActiveRecord::Migration
  def change
    create_table :attribute_items do |t|
      t.integer :attribute_id
      t.string :name

      t.timestamps
    end
  end
end
