class AddTitleKeywordsDescriptionToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :html_title, :string
    add_column :categories, :html_keywords, :string
    add_column :categories, :html_description, :string
  end
end
