class AddSequenceIdToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :sequence_id, :integer
  end
end
