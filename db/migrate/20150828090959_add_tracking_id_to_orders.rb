class AddTrackingIdToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :tracking_id, :string
  end
end
