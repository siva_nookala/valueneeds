class CreateOrderStatusHistories < ActiveRecord::Migration
  def change
    create_table :order_status_histories do |t|
      t.integer :order_id
      t.integer :user_id
      t.string :status_type
      t.string :old_status
      t.string :new_status

      t.timestamps
    end
  end
end
