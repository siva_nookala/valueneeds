class CreateGalleries < ActiveRecord::Migration
  def change
    create_table :galleries do |t|
      t.string :title
      t.datetime :date
      t.string :event_name
      t.string :picture
      t.text :description

      t.timestamps
    end
  end
end
