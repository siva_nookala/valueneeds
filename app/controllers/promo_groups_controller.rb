class PromoGroupsController < ApplicationController
  filter_resource_access
  # GET /promo_groups
  # GET /promo_groups.json
  def index
    @promo_groups = PromoGroup.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @promo_groups }
    end
  end

  # GET /promo_groups/1
  # GET /promo_groups/1.json
  def show
    @promo_group = PromoGroup.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @promo_group }
    end
  end

  # GET /promo_groups/new
  # GET /promo_groups/new.json
  def new
    @promo_group = PromoGroup.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @promo_group }
    end
  end

  # GET /promo_groups/1/edit
  def edit
    @promo_group = PromoGroup.find(params[:id])
  end

  # POST /promo_groups
  # POST /promo_groups.json
  def create
    @promo_group = PromoGroup.new
    update_group_category
    respond_to do |format|
      if @promo_group.save
        format.html { redirect_to @promo_group, notice: 'Promo group was successfully created.' }
        format.json { render json: @promo_group, status: :created, location: @promo_group }
      else
        format.html { render action: "new" }
        format.json { render json: @promo_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /promo_groups/1
  # PUT /promo_groups/1.json
  def update
    @promo_group = PromoGroup.find(params[:id])
    update_group_category
    respond_to do |format|
      if @promo_group.save
        format.html { redirect_to @promo_group, notice: 'Promo group was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @promo_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /promo_groups/1
  # DELETE /promo_groups/1.json
  def destroy
    @promo_group = PromoGroup.find(params[:id])
    @promo_group.destroy

    respond_to do |format|
      format.html { redirect_to promo_groups_url }
      format.json { head :ok }
    end
  end

  def update_group_category
    @promo_group.title = params[:promo_group][:title]
    @promo_group.description = params[:promo_group][:description]
    @promo_group.active = params[:promo_group][:active]
    uploaded_io = params[:promo_group][:image]
    if uploaded_io
      file_path_name = UUIDTools::UUID.random_create.to_s.delete '-'
      just_filename = File.basename(uploaded_io.original_filename)
      file_path_name += just_filename.sub(/[^\w\.\-]/, '_')
      File.open(Rails.root.join('public', 'product-group', file_path_name), 'w+b') do |file|
        file.write(uploaded_io.read)
      end
      puts "-----------------"
      puts file_path_name
      @promo_group.image = file_path_name
    end
  end
end
