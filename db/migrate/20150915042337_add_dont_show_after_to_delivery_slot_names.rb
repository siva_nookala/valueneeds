class AddDontShowAfterToDeliverySlotNames < ActiveRecord::Migration
  def change
    add_column :delivery_slot_names, :dont_show_after, :datetime
  end
end
