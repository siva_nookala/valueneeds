class CategoriesController < ApplicationController
  filter_resource_access
  filter_access_to :get_child_categories, :attribute_check => false
  filter_access_to :update_sequence_ids, :attribute_check => false
  filter_access_to :change_category_sequence_order, :attribute_check => false
  include CategoriesHelper
  # GET /categories
  # GET /categories.json
  def index
    @categories = Category.find(:all, :conditions => ['parent_id is null'], :order => "sequence_id ASC")

    respond_to do |format|
      format.html # index.html.erb
      @categories.each do |category|
        category.picture = root_url+'category/'+category.picture if !category.picture.blank?
      end
      format.json { render json: @categories }
    end
  end

  # GET /categories/1
  # GET /categories/1.json
  def show
    @category = Category.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @category }
    end
  end

  # GET /categories/new
  # GET /categories/new.json
  def new
    @category = Category.new
    @category.parent_id = params[:parent_id] if !params[:parent_id].blank?

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @category }
    end
  end

  # GET /categories/1/edit
  def edit
    @category = Category.find(params[:id])
  end

  # POST /categories
  # POST /categories.json
  def create
    @category = Category.new
    update_category
    respond_to do |format|
      if @category.save
        format.html { redirect_to @category, notice: 'Category was successfully created.' }
        format.json { render json: @category, status: :created, location: @category }
      else
        format.html { render action: "new" }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /categories/1
  # PUT /categories/1.json
  def update
    @category = Category.find(params[:id])
    update_category
    respond_to do |format|
      if @category.save
        format.html { redirect_to @category, notice: 'Category was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categories/1
  # DELETE /categories/1.json
  def destroy
    @category = Category.find(params[:id])
    @category.destroy

    respond_to do |format|
      format.html { redirect_to categories_url }
      format.json { head :ok }
    end
  end

  def update_category
    @category.parent_id = params[:category][:parent_id]
    @category.name = params[:category][:name]
    @category.active = params[:category][:active]
    @category.show_on_top = params[:category][:show_on_top]
    @category.is_popular = params[:category][:is_popular]
    @category.sequence_id = @category.id
    @category.description = params[:category][:description]
    @category.html_title = params[:category][:html_title]
    @category.html_keywords = params[:category][:html_keywords]
    @category.html_description = params[:category][:html_description]

    uploaded_io = params[:category][:picture]
    if uploaded_io
      file_path_name = UUIDTools::UUID.random_create.to_s.delete '-'
      just_filename = File.basename(uploaded_io.original_filename)
      file_path_name += just_filename.sub(/[^\w\.\-]/, '_')
      File.open(Rails.root.join('public', 'category', file_path_name), 'w+b') do |file|
        file.write(uploaded_io.read)
      end
      @category.picture = file_path_name
    end
  end

  def get_child_categories
    categories = []
    CategoriesHelper.get_categories_by_parent(params[:parent_id], categories)
    @category_ids = categories.collect{|category| category.id}
    respond_to do |format|
      categories.each do |category|
        category.picture = root_url+'category/'+category.picture if !category.picture.blank?
      end
      format.json { render json: categories }
    end
  end

  def update_sequence_ids
     @categories = Category.all
     @categories.each do |category|
       category.sequence_id = category.id
       category.save
     end
    redirect_to "/categories"
  end

  def change_category_sequence_order
      @category1 = Category.find_by_sequence_id(params[:c_sq_id])
      @category2 = Category.find_by_sequence_id(params[:rp_sq_id])
      @category1.sequence_id = params[:rp_sq_id]
      @category1.save
      @category2.sequence_id = params[:c_sq_id]
      @category2.save
      redirect_to "/categories"
  end


end
