class CreateNotifyMeRequests < ActiveRecord::Migration
  def change
    create_table :notify_me_requests do |t|
      t.integer :product_id
      t.integer :user_id
      t.string :intimation_type

      t.timestamps
    end
  end
end
