class AddOutOfStockToProducts < ActiveRecord::Migration
  def change
    add_column :products, :out_of_stock, :boolean, :default => 'false'
  end
end
