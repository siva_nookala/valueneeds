class AddHasAttributeToProducts < ActiveRecord::Migration
  def change
    add_column :products, :is_attribute, :boolean,        :default => false
  end
end
