class CreateBottomBanners < ActiveRecord::Migration
  def change
    create_table :bottom_banners do |t|
      t.string :url
      t.string :image

      t.timestamps
    end
  end
end
