class CreateAddressDetails < ActiveRecord::Migration
  def change
    create_table :address_details do |t|
      t.string :first_name
      t.string :last_name
      t.string :contact_number
      t.string :house_no
      t.string :area
      t.string :pincode
      t.string :city
      t.string :land_mark
      t.integer :buyer_id

      t.timestamps
    end
  end
end
