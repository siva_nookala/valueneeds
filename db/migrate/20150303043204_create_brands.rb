class CreateBrands < ActiveRecord::Migration
  def change
    create_table :brands do |t|
      t.string :name
      t.text :description
      t.string :picture
      t.boolean :active

      t.timestamps
    end
  end
end
