class AddOrderIdToWalletEntries < ActiveRecord::Migration
  def change
    add_column :wallet_entries, :order_id, :string
  end
end
