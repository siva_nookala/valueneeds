class AddPreOrderToProducts < ActiveRecord::Migration
  def change
    add_column :products, :pre_order, :boolean, :default => false
  end
end
