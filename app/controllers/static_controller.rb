class StaticController < ApplicationController
  include StaticHelper

  def contact_us
    @system_setting = SystemSetting.find_by_key('contact-us')
  end

  def about_us
    @system_setting = SystemSetting.find_by_key('about-us')
  end

  def faq
    @system_setting = SystemSetting.find_by_key('faq')
  end

  def privacy_policy
    @system_setting = SystemSetting.find_by_key('privacy-policy')
  end

  def terms_and_conditions
    @system_setting = SystemSetting.find_by_key('terms-conditions')
  end

  def delivery_information
    @system_setting = SystemSetting.find_by_key('delivery-information')
  end

  def refund_and_return_policy
    @system_setting = SystemSetting.find_by_key('refund-return-policy')
  end

  def follow_us

  end

  def where_we_deliver
    @system_setting = SystemSetting.find_by_key('what-we-deliver')
  end

  def site_map

  end

  def general_information
    @system_setting = SystemSetting.find_by_key('general-information')
  end

  def need_assistance
    @system_setting = SystemSetting.find_by_key('need-assistance')
  end

  def customer_service
    @system_setting = SystemSetting.find_by_key('customer-service')
  end

  def getting_started
    @system_setting = SystemSetting.find_by_key('getting-started')
  end

  def vn_offers
    @system_setting = SystemSetting.find_by_key('vn-offers')
  end

  def coupon_and_vouchers
    @system_setting = SystemSetting.find_by_key('coupon-vouchers')
  end

  def news_and_events
    @system_setting = SystemSetting.find_by_key('news-events')
  end

  def winners
    @system_setting = SystemSetting.find_by_key('winners')
  end

  def dataFrom
  end

  def ccavRequestHandler
    merchantData=""
    working_key="7F2F4E95C75D2051350DD64BF2E0FE17" #Put in the 32 Bit Working Key provided by CCAVENUES.
    @access_code="AVEI06CI26AB94IEBA" #Put in the Access Code in quotes provided by CCAVENUES.
    params.each do |key, value|
      merchantData += key+"="+value+"&"
    end
    @encrypted_data = encrypt(merchantData, working_key)

    respond_to do |format|
      format.html { render "static/ccavRequestHandler", :layout => false }
    end

  end

  def payment_response_handler
    #Create new map call it ccavenueParams
    workingKey="7F2F4E95C75D2051350DD64BF2E0FE17" #Put in the 32 Bit Working Key provided by CCAVENUES.
    encResponse=params[:encResp]
    decResp=decrypt(encResponse, workingKey);
    decResp = decResp.split("&")
    @ccavenueParams = {}
    decResp.each do |key|
      @ccavenueParams[key.from(0).to(key.index("=")-1)] = key.from(key.index("=")+1).to(-1)
    end
    @online_payment = OnlinePayment.find_by_merchant_transaction_id(@ccavenueParams["order_id"])
    if !@online_payment.blank?
      @online_payment.payment_status = @ccavenueParams["order_status"]
      @online_payment.gateway_amount_recieved = @ccavenueParams["amount"].to_f
      @online_payment.transaction_details = @ccavenueParams
      @online_payment.save

      if @online_payment.payment_status.eql?('Success')
        if @online_payment.wallet_amount > 0
          @wallet_entry = WalletEntry.new
          @wallet_entry.user_id = current_user.id
          @wallet_entry.amount = -@online_payment.wallet_amount
          @wallet_entry.reason = 'Paid for order'
          @wallet_entry.transaction_id = @online_payment.merchant_transaction_id
          @wallet_entry.save!
        end
        redirect_to "/address_details/create_order_from_payment_gateway?mti=#{@online_payment.merchant_transaction_id}"
      else
        redirect_to '/products/show_cart'
      end
    end
  end
end
