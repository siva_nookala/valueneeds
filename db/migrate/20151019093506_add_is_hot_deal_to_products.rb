class AddIsHotDealToProducts < ActiveRecord::Migration
  def change
    add_column :products, :is_hot_deal, :boolean, :default => false
  end
end
