class ModifyTransactionIdInWalletEntries < ActiveRecord::Migration
  def up
    change_column :wallet_entries, :transaction_id, :string
  end

  def down
    change_column :wallet_entries, :transaction_id, :integer
  end
end
