class CreateOnlinePayments < ActiveRecord::Migration
  def change
    create_table :online_payments do |t|
      t.string :merchant_transaction_id
      t.decimal :transaction_amount
      t.integer :order_id
      t.integer :address_id
      t.string :payment_status
      t.text :transaction_details

      t.timestamps
    end
  end
end
