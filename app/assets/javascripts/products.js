// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

(function ($) {

    // Log all jQuery AJAX requests to Google Analytics
    $(document).ajaxSend(function (event, xhr, settings) {
        if (typeof _gaq !== "undefined" && _gaq !== null) {
            _gaq.push(['_trackPageview', settings.url]);
        }
    });


})(jQuery);

function fly_to_kart(product_id) {
    var cart = $('.cart_image2');
    var final_width = 0;
    if( $( document ).width() > 650)
    {
        cart = $('.cart_image');
        final_width = 40;
    }

    var product_image_div = $('#product_image_div_' + product_id);
    if (product_image_div) {
        var imgtodrag = product_image_div.find("img").eq(0);
        if (imgtodrag) {
            var imgclone = imgtodrag.clone()
                .offset({
                    top: imgtodrag.offset().top,
                    left: imgtodrag.offset().left
                })
                .css({
                    'opacity': '0.5',
                    'position': 'absolute',
                    'width': '150px',
                    'z-index': '100'
                })
                .appendTo($('body'))
                .animate({
                    'top': cart.offset().top + ($( document ).width() > 650 ? 20 : 10),
                    'left': cart.offset().left + ($( document ).width() > 650 ? 30 : 10),
                    'width': ($( document ).width() > 650 ? 75 : 30),
                    'height': ($( document ).width() > 650 ? 75 : 30)
                }, 1500, 'easeInOutExpo');

            setTimeout(function () {
                cart.effect("shake", {
                    times: 2
                }, 200);
            }, 1500);

            imgclone.animate({
                'width': final_width,
                'height': final_width
            }, function () {
                $(this).detach()
            });
        }
    }

}