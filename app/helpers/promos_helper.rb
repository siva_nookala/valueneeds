module PromosHelper
  class PromosItem
    attr_accessor :promo_id, :promo_title, :promo_quantity, :offer_type, :offer_product_id, :offer_product_quantity, :discount_percentage, :discount_amount, :order_promo_limit, :savings_amount, :offer_limit_quantity
  end

  class CartOrderItem
    attr_accessor :product, :category, :order_item, :promo
  end

  class CategoryCartLineItem
    attr_accessor :type, :category_id, :category_name, :count, :total_cost, :savings
    def initialize
      @type = 'Category'
      @count = @total_cost = @savings = 0.0
    end
  end

  class ProductCartLineItem
    attr_accessor :type, :product_id, :product_name, :quantity, :original_price, :discounted_price, :effective_price, :sub_total, :savings, :product_attributes, :picture
    def initialize
      @type = 'Product'
      @quantity = @original_price = @discounted_price = @effective_price = @sub_total = @savings = 0
    end
  end

  class PromoCartLineItem
    attr_accessor :type, :promo_id, :description, :savings, :price, :discounted_price, :applied_promo_quantity
    def initialize
      @type = 'Promo'
      @savings = @price = @discounted_price = @applied_promo_quantity = 0
    end
  end

  class OrderItemData
    attr_accessor :product_id, :product_name, :quantity, :original_price, :effective_price, :sub_total, :discounted_price, :order_index, :product_attributes
  end

  class CategoryWiseList
    attr_accessor :cart_order_items, :category, :promo
  end

  class PromoOfferDetail
    attr_accessor :promo_id, :content1, :content2, :content3, :content4, :free_product_id, :all_product_ids
    def initialize
      @free_product_id = nil
    end
  end

  class PromoBasketDetail
    attr_accessor :name, :content1, :content2, :content3, :content4, :content5
  end

  def get_all_promo_types
    ['On Order Value','On Product','On Product Range']
  end

  def get_promo_offer_types
    ['Discount Amount','Discount Percentage','Add Other Product', 'Add Same Product']
  end

  def get_cart_by_category_level(session_order_items)
    list_of_categories = Array.new
    order_items = get_order_items_by_order(session_order_items)
    items_lists = get_cart_list_category_group_wise(order_items)
    if !items_lists.blank?
       items_lists.each do|list|
         category_wise_list = CategoryWiseList.new
         category_wise_list.cart_order_items = list
         list.each_with_index do|order_item,index|
            break if index > 0
            category_wise_list.category = order_item.category
         end
=begin
         promo_range = Promo.find_by_product_range_id(category_wise_list.category.id)
         if !promo_range.blank?
           promo_item = is_category_has_promo_range(promo_range,list)
           if !promo_item.blank?
             category_wise_list.promo = promo_item
           end
         end
=end
         list_of_categories << category_wise_list
       end
    end
   list_of_categories
  end

  def get_cart_list_category_group_wise(order_items)
     list_items = cart_temp_list(order_items)
     #list_items = list_items.sort! { |a, b| a.category.id <=> b.category.id }
     objects_lists = Array.new
     list_items.each_with_index do |order_item|
        group = is_category_in_list(objects_lists,order_item.category)
        list = Array.new
        if !group.blank?
        list = group
        list << order_item
        else
        objects_lists << list
        list << order_item
        end
     end
    objects_lists
  end


  def is_category_in_list(objects_lists, category)
      result = nil
      if !objects_lists.blank?
         objects_lists.each do |list|
           if !list.blank?
            list.each do |order_item|
                  if order_item.category.id == category.id
                       result = list
                  end
              end
           end
         end
      end
     result
  end

  def cart_temp_list(order_items)
    objects_lists = Array.new
        order_items.each_with_index do |order_item, index|
            product = Product.find_by_id(order_item.item_id)
            product_category = ProductCategory.find_by_product_id(product.id)
            category = !product_category.blank? ? Category.find_by_id(product_category.category_id) : Category.find_by_id(541)
            category = get_parent_level_category(category)
            order_item_obj = CartOrderItem.new
            order_item_obj.product = product
            order_item_obj.category = category
            order_item_data_obj = OrderItemData.new
            order_item_data_obj.product_id = order_item.item_id
            order_item_data_obj.product_name = product.name
            order_item_data_obj.quantity = order_item.quantity
            order_item_data_obj.original_price = order_item.price
            order_item_data_obj.product_attributes = order_item.product_attributes if !order_item.product_attributes.blank?
            order_item_data_obj.effective_price = order_item.price - get_discount_product_price(product)
            order_item_data_obj.order_index = index
            order_item_data_obj.sub_total = (order_item.quantity.to_i*order_item_data_obj.effective_price.to_d).round(2)
            #............promo calculations
            promo = is_item_has_promo(order_item_data_obj)
             if !promo.blank?
               order_item_obj.promo = promo
               order_item_data_obj.sub_total = (order_item_data_obj.sub_total - promo.savings_amount).round(2) if promo.offer_type != 'Add Other Product' && promo.offer_type != 'Add Same Product'
               order_item_data_obj.effective_price = (order_item_data_obj.sub_total/order_item_data_obj.quantity.to_i).round(2)
             end
           order_item_obj.order_item = order_item_data_obj
        objects_lists << order_item_obj
        end
    objects_lists
  end

  def get_parent_level_category(category)
      result = category
      if !category.parent_id.blank?
         parent_category = Category.find_by_id(category.parent_id)
         result = get_parent_level_category(parent_category) if !parent_category.blank?
      end
    result
  end

  def get_order_items_by_order(order_items)
    list_of_order_items = Array.new
    order_items.each do |order_item|
      product_category = ProductCategory.find_by_product_id(order_item.item_id)
      category = !product_category.blank? ? Category.find_by_id(product_category.category_id) : Category.find_by_id(541)
      category_id = get_parent_level_category(category).id
      list_of_order_items << [order_item, category_id]
    end
    list_of_order_items.sort! { |a, b| a[1] <=> b[1] }
    list_of_order_items.collect { |order_item| order_item[0] }
  end

  def get_cart_line_items(session_order_items)
    objects_lists = Array.new
    last_category_item = nil
    order_items = get_order_items_by_order(session_order_items)
    order_items.each_with_index do |order_item, index|
      product = Product.find_by_id(order_item.item_id)
      product_category = ProductCategory.find_by_product_id(product.id)
      product_category_id = !product_category.blank? ? product_category.category_id : 541
      category =  get_parent_level_category(Category.find_by_id(product_category_id))
      if last_category_item.nil? || last_category_item.category_id != category.id
        last_category_item = CategoryCartLineItem.new
        last_category_item.category_id = category.id
        last_category_item.category_name = category.name
        objects_lists << last_category_item
      end
      last_category_item.count += 1
      product_line_item = ProductCartLineItem.new
      product_line_item.product_id = product.id
      product_line_item.product_name = product.name
      product_line_item.picture = root_url+'product/'+product.picture if !product.picture.blank?
      product_line_item.quantity = order_item.quantity
      product_line_item.original_price = order_item.price
      product_line_item.product_attributes = order_item.product_attributes if !order_item.product_attributes.blank?
      product_line_item.effective_price = product.price - get_discount_product_price(product)
      product_line_item.sub_total = (order_item.quantity.to_i*product_line_item.effective_price.to_d).round(2)
      product_line_item.savings = get_discount_product_price(product)*product_line_item.quantity.to_i
      objects_lists << product_line_item
      ##............promo calculations
      promo = is_item_has_promo(product_line_item)
      if !promo.blank?
        promo_line_item = PromoCartLineItem.new
        promo_line_item.promo_id = promo.promo_id
        promo_line_item.description = promo.promo_title
        promo_line_item.savings = promo.savings_amount
        promo_line_item.applied_promo_quantity = promo.offer_limit_quantity
        if promo.offer_type == 'Discount Percentage' || promo.offer_type == 'Discount Amount'
          product_line_item.sub_total = (product_line_item.sub_total - promo.savings_amount).round(2)
          product_line_item.effective_price = (product_line_item.sub_total/product_line_item.quantity.to_i).round(2)
        end
        objects_lists << promo_line_item
      end
      last_category_item.total_cost += product_line_item.sub_total
    end
    objects_lists
  end


  def is_item_has_promo(order_item)
    promo = Promo.find_by_product_id(order_item.product_id)
    promo_item = PromosItem.new
    if !promo.blank? && order_item.quantity.to_i >= promo.product_quantity.to_i
          promo_item.promo_id = promo.id
          promo_item.promo_title = promo.promo_title
          promo_item.offer_type = promo.offer_type
          promo_item.offer_limit_quantity = (order_item.quantity.to_i/promo.product_quantity.to_i)
          promo_item.offer_limit_quantity = promo.order_promo_limit if (promo_item.offer_limit_quantity > promo.order_promo_limit)
          if promo_item.offer_type == 'Discount Percentage'
             promo_item.discount_percentage = promo.discount_percent
             promo_item.promo_quantity = promo.product_quantity
             promo_item.order_promo_limit = promo.order_promo_limit
          elsif promo_item.offer_type == 'Discount Amount'
             promo_item.discount_amount = promo.discount_amount
             promo_item.promo_quantity = promo.product_quantity
             promo_item.order_promo_limit = promo.order_promo_limit
          elsif promo_item.offer_type == 'Add Other Product'
             promo_item.offer_product_id = promo.offer_product_id
             promo_item.offer_product_quantity = promo.offer_other_product_quantity
             promo_item.promo_quantity = promo.product_quantity
             promo_item.order_promo_limit = promo.order_promo_limit
          elsif promo_item.offer_type == 'Add Same Product'
            promo_item.offer_product_id = promo.product_id
            promo_item.offer_product_quantity = promo.offer_same_product_quantity
            promo_item.promo_quantity = promo.product_quantity
            promo_item.order_promo_limit = promo.order_promo_limit
          end
          get_the_savings_amount(promo_item,promo,order_item)
    else
    promo_item = nil
    end
    promo_item
  end

  def is_category_has_promo_range(promo_range, list_items)
    promo_product_maps = ProductPromoMapping.find_all_by_promo_id(promo_range.id)
    order_items = Array.new
    promo_product_maps.each do |promo_map|
      order_item = is_cart_has_promo_range_item(promo_map.product_id, list_items)
      order_items << order_item if !order_item.blank?
    end
    total_order_quantity = 0
    order_items.each do |order_item|
     total_order_quantity += order_item.quantity.to_i
    end
    promo_item = PromosItem.new
    if total_order_quantity >= promo_range.product_quantity.to_i
      promo_item.promo_id = promo_range.id
      promo_item.promo_title = promo_range.promo_title
      promo_item.offer_type = promo_range.offer_type
      promo_item.offer_limit_quantity = (total_order_quantity.to_i/promo_range.product_quantity.to_i)
      promo_item.offer_limit_quantity = promo_range.order_promo_limit if (promo_item.offer_limit_quantity > promo_range.order_promo_limit)
      if promo_item.offer_type == "Discount Percentage"
        promo_item.discount_percentage = promo_range.discount_percent
        promo_item.promo_quantity = promo_range.product_quantity
        promo_item.order_promo_limit = promo_range.order_promo_limit
      elsif promo_item.offer_type == "Discount Amount"
        promo_item.discount_amount = promo_range.discount_amount
        promo_item.promo_quantity = promo_range.product_quantity
        promo_item.order_promo_limit = promo_range.order_promo_limit
      elsif promo_item.offer_type == "Add Other Product"
        promo_item.offer_product_id = promo_range.offer_product_id
        promo_item.offer_product_quantity = promo_range.offer_other_product_quantity
        promo_item.promo_quantity = promo_range.product_quantity
        promo_item.order_promo_limit = promo_range.order_promo_limit
      elsif promo_item.offer_type == "Add Same Product"
        promo_item.offer_product_id = promo_range.product_id
        promo_item.offer_product_quantity = promo_range.offer_same_product_quantity
        promo_item.promo_quantity = promo_range.product_quantity
        promo_item.order_promo_limit = promo_range.order_promo_limit
      end
      get_the_savings_amount_for_promo_range(promo_item)
    else
      promo_item = nil
    end
    promo_item
  end

  def get_the_savings_amount_for_promo_range(promo_item)
    if promo_item.offer_type == "Add Other Product" || promo_item.offer_type == "Add Same Product"
      offer_product = Product.find_by_id(promo_item.offer_product_id)
      promo_item.savings_amount = offer_product.price.to_d* promo_item.offer_limit_quantity.to_i
    end
  end

  def is_cart_has_promo_range_item(product_id, list_items)
    result = nil
    list_items.each do |list_item|
      result = list_item.order_item if list_item.order_item.product_id == product_id
    end
    result
  end


  def get_the_savings_amount(promo_item,promo,order_item)
    if promo_item.offer_type == "Add Other Product" || promo_item.offer_type == "Add Same Product"
      offer_product = Product.find_by_id(promo_item.offer_product_id)
      promo_item.savings_amount = offer_product.price.to_d * promo_item.offer_limit_quantity.to_i
    end
    if promo_item.offer_type == "Discount Amount"
      promo_item.savings_amount = (promo_item.offer_limit_quantity.to_i * promo_item.promo_quantity.to_i * promo_item.discount_amount.to_d)
      order_item.discounted_price = order_item.original_price.to_d - promo.discount_amount.to_d
    end
    if promo_item.offer_type == "Discount Percentage"
      product = Product.find_by_id(promo.product_id)
      discount_amount = ((order_item.original_price.to_d/100.0)*promo_item.discount_percentage.to_i)
      promo_item.savings_amount = (promo_item.offer_limit_quantity.to_i * promo_item.promo_quantity.to_i * discount_amount)
      order_item.discounted_price = order_item.original_price.to_d - discount_amount
    end
  end

  def total_price_of_cart(category_lists)
      total_price = 0.0
      category_lists.each do |category_list|
        category_list.cart_order_items.each do |list|
          total_price += list.order_item.sub_total
        end
      end
      total_price
  end

  def offer_rename(name)
     name.capitalize.gsub(' ','-')
  end

  def get_promo_details(promo)
     promo_offer_details = PromoOfferDetail.new
     promo_offer_details.promo_id = promo.id
     all_products = Product.where('id in (select product_id from product_promo_mappings where promo_id is ?)', promo.id)
     product = all_products[0]
     promo_offer_details.all_product_ids = all_products.collect{ |product| product.id}
     promo_offer_details.content1 = promo.promo_title
     promo_offer_details.content2 = "Buy #{promo.product_quantity} #{product.name}"
     content3 = ""
     free_product = nil
         if promo.offer_type == 'Discount Amount'
            content3 ="Get Rs. #{promo.discount_amount}/- Off on MRP"
         elsif promo.offer_type == 'Discount Percentage'
          content3 ="Get Rs.#{promo.discount_percent}% Off on MRP"
         elsif promo.offer_type == 'Add Other Product'
          free_product = Product.find(promo.offer_product_id)
          content3 = "Get #{free_product.name.truncate(10)} Free"
         elsif promo.offer_type == 'Add Same Product'
            free_product = product
            content3 = "Get #{product.name} Free"
         end
     promo_offer_details.content3 = content3
     promo_offer_details.content4 = "* Promotion valid till #{promo.valid_till}. Limit: #{promo.order_promo_limit} per order, #{promo.customer_promo_limit} per customer."
     promo_offer_details.free_product_id = free_product.id if !free_product.blank?
     promo_offer_details
  end

  def get_promo_products_details(promo)
    all_products = Product.where('id in (select product_id from product_promo_mappings where promo_id is ?)', promo.id)
    product = all_products[0]
    cart_items = !current_user.blank? ? CartItem.where('item_id in (?) and user_id =?', all_products.collect{|product| product.id}, current_user.id) : CartItem.where('item_id in (?) and session_id =?', all_products.collect{|product| product.id}, session[ApplicationHelper::SessionConstants::SESSION_ID])
    basket_size = 0
    cart_items.each{|cart_item| basket_size += cart_item.quantity.to_i}
    promo_basket_detail = PromoBasketDetail.new
    promo_basket_detail.name = product.name.truncate(10)
    promo_basket_detail.content1 = "#{basket_size} added to basket"
    number_of_promo_used = (basket_size/promo.product_quantity.to_i)
    number_of_promo_used = promo.order_promo_limit if (number_of_promo_used > promo.order_promo_limit)
    if basket_size == 0
      promo_basket_detail.content2 = "You have not added any products yet. Add some now."
    elsif number_of_promo_used < 1
    promo_basket_detail.content2 = "Add #{promo.product_quantity.to_i - basket_size} more quantity of #{product.name.truncate(10)}"
    elsif number_of_promo_used >= promo.order_promo_limit.to_i
    promo_basket_detail.content2 = "#{number_of_promo_used} promos are added to your basket. You have reached the limit of #{promo.order_promo_limit.to_i} promos per order."
    else
    promo_basket_detail.content2 = "#{number_of_promo_used} promos are added to your basket. You can add #{promo.order_promo_limit.to_i - number_of_promo_used} more in this order."
    end
    promo_basket_detail.content3 = "#{number_of_promo_used} used promos"
    savings_amount = 0.0
    if promo.offer_type == "Add Other Product" || promo.offer_type == "Add Same Product"
      free_product = Product.find_by_id(promo.offer_product_id) if promo.offer_type == "Add Other Product"
      free_product = product if promo.offer_type == "Add Same Product"
      savings_amount = free_product.price.to_d * number_of_promo_used
    elsif promo.offer_type == "Discount Amount"
      savings_amount = (number_of_promo_used * promo.product_quantity.to_i * promo.discount_amount.to_d)
    elsif promo.offer_type == "Discount Percentage"
      discount_amount = ((product.price.to_d/100.0)*promo.discount_percent.to_i)
      savings_amount = (number_of_promo_used * promo.product_quantity.to_i * discount_amount)
    end
    promo_basket_detail.content4 = "You've saved Rs #{savings_amount}"
    promo_basket_detail
  end

end
