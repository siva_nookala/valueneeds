class AddProductPriceRangeIdToProducts < ActiveRecord::Migration
  def change
    add_column :products, :product_price_range_id, :integer
  end
end
