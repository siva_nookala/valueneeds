class UserSessionsController < ApplicationController
  require 'net/http'
  require 'json'
  include ApplicationHelper
  skip_before_filter :authorize


  def new
    @user_session = UserSession.new
    @user = User.new
  end

  def create
    @user_session = UserSession.new(params[:user_session])
    @user = User.new
    if @user_session.save
      flash[:notice] = 'Successfully logged In.'
      redirect_to_previous_url
    else
      render :action => 'new'
    end
  end

  def log_in_with_others
    @current_user = User.find_or_create_from_oauth(env["omniauth.auth"])
    if @current_user
      @user_session = UserSession.create(@current_user, true)
      @user_session.save
      update_cart_items
      redirect_to_previous_url
    end
  end


  def log_in_with_email
    if User.is_valid_login(params)
      @user_access_token = User.find_or_create_from_email(params)
      if @user_access_token
        if request.format == 'json'
          render :json => {:user_access_token => @user_access_token.access_token}
        end
      end
    else
      if request.format == 'json'
        render :json => 'Invalid login details'
      else
        redirect_to_previous_url
      end
    end
  end

  def update_cart_items
    if !current_user.blank?
      if !session[ApplicationHelper::SessionConstants::SESSION_ID].blank?
        @cart_items = CartItem.find_all_by_session_id(session[ApplicationHelper::SessionConstants::SESSION_ID])
        @cart_items.each do |item|
          item.user_id = current_user.id
          item.save
        end
      end
    end
  end

  def destroy
    @user_session = UserSession.find
    if !@user_session.nil?
      @user_session.destroy
      flash[:notice] = "Successfully logged out"
    end
    redirect_to root_url
  end

  def redirect_to_previous_url
    if !session['REDIRECT_URL'].blank?
      redirect_to session['REDIRECT_URL']
      session['REDIRECT_URL'] = nil
    else
      redirect_to root_url
    end
  end

end
