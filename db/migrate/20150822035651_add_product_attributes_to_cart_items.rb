class AddProductAttributesToCartItems < ActiveRecord::Migration
  def change
    add_column :cart_items, :product_attributes, :string
  end
end
