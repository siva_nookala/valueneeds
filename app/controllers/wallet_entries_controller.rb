class WalletEntriesController < ApplicationController
  filter_resource_access
  filter_access_to  :my_wallet, :attribute_check => false
  filter_access_to  :show_wallet, :attribute_check => false
  # GET /wallet_entries
  # GET /wallet_entries.json
  def index
    @wallet_entries = WalletEntry.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @wallet_entries }
    end
  end

  # GET /wallet_entries/1
  # GET /wallet_entries/1.json
  def show
    @wallet_entry = WalletEntry.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @wallet_entry }
    end
  end

  # GET /wallet_entries/new
  # GET /wallet_entries/new.json
  def new
    @wallet_entry = WalletEntry.new
    @wallet_entry.user_id = params[:user_id]

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @wallet_entry }
    end
  end

  # GET /wallet_entries/1/edit
  def edit
    @wallet_entry = WalletEntry.find(params[:id])
  end

  # POST /wallet_entries
  # POST /wallet_entries.json
  def create
    @wallet_entry = WalletEntry.new(params[:wallet_entry])

    respond_to do |format|
      if @wallet_entry.save
        format.html { redirect_to '/wallet_entries/show_wallet?user_id=' + @wallet_entry.user_id.to_s }
        format.json { render json: @wallet_entry, status: :created, location: @wallet_entry }
      else
        format.html { render action: "new" }
        format.json { render json: @wallet_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /wallet_entries/1
  # PUT /wallet_entries/1.json
  def update
    @wallet_entry = WalletEntry.find(params[:id])

    respond_to do |format|
      if @wallet_entry.update_attributes(params[:wallet_entry])
        format.html { redirect_to @wallet_entry, notice: 'Wallet entry was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @wallet_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /wallet_entries/1
  # DELETE /wallet_entries/1.json
  def destroy
    @wallet_entry = WalletEntry.find(params[:id])
    @wallet_entry.destroy

    respond_to do |format|
      format.html { redirect_to wallet_entries_url }
      format.json { head :ok }
    end
  end

  def show_wallet
    @wallet_entries = WalletEntry.find_all_by_user_id(params[:user_id])

    respond_to do |format|
      format.html
      format.json
    end
  end

  def my_wallet
    @wallet_entries = WalletEntry.find_all_by_user_id(current_user.id)

    respond_to do |format|
      format.html
      format.json
    end
  end
end
