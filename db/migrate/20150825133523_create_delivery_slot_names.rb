class CreateDeliverySlotNames < ActiveRecord::Migration
  def change
    create_table :delivery_slot_names do |t|
      t.string :slot_name
      t.boolean :active

      t.timestamps
    end
  end
end
