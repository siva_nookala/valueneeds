class Feedback < ActiveRecord::Base
  validates_presence_of :comment, :user_name, :email, :mobile_number
end
