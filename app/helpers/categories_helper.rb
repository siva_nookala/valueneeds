module CategoriesHelper
  def get_category_type_html(category, level, colors, index, level_categories)
    font_size = 20-level
    indent = 40*level
    color = colors[level%colors.size]
    result = "<tr><td style=\"font-size:"+font_size.to_s+"px;padding-left:"+(50+indent).to_s+"px;\">
              <table style=\"border-radius:5px;padding:5px 10px;border:2px solid#{color};border-left:10px solid #{color};\">
              <td width ='35%'style='font-size:12px'>#{category.name}</td>
               <td width ='21%'>#{image_tag("/category/#{category.picture}", :width => '50px')}</td>
              <td width ='10%'><a href='/categories/#{category.id}/edit' class='button_medium_sty'>#{image_tag('edit.png', :width => '17px')}</a></td><td width ='10%'><a href='/categories/new?parent_id=#{category.id}'>#{image_tag('add-icon.png', :width => '35px')}</a></td>
              <td width ='8%'>#{link_to image_tag('delete.png', :width => "17px"), confirm: 'Are you sure?', method: :delete}</td>
              <td width ='8%'>"
    if index != 0
      replace_sequence_id = level_categories[index-1].sequence_id
      result += "<a href='/categories/change_category_sequence_order?c_sq_id=#{category.sequence_id}&rp_sq_id=#{replace_sequence_id}'>#{image_tag('move_up.png', :width => '20px')}</a>"
    end
    result += "</td><td width ='8%'>"
    if index+1 != level_categories.size
      replace_sequence_id = level_categories[index+1].sequence_id
      result += "<a href='/categories/change_category_sequence_order?c_sq_id=#{category.sequence_id}&rp_sq_id=#{replace_sequence_id}'>#{image_tag('move_down.png', :width => '20px')}</a>"
    end
    result += "</td></table></td></tr>"
    children = Category.find(:all, :conditions => ['parent_id =?', category.id], :order => "sequence_id ASC")
    if !children.blank? && children.size>0
      children.each_with_index do |child_category_type, index|
        result += get_category_type_html(child_category_type, level+1, colors, index, children)
      end
    end
    result
  end

  def self.get_category_type_titles(selected_category_type)
    result=[]
    if selected_category_type.blank? || selected_category_type.id.blank?
      categories = Category.find(:all, :conditions => ['parent_id is null'])
    else
      categories =Category.find(:all, :conditions => ['parent_id is null and id != ?', selected_category_type.id])
    end
    categories.each_with_index do |category, index|
      get_indented_category_titles(category, 0, result, selected_category_type)
    end
    result
  end

  def self.get_indented_category_titles(category, level, result, selected_category_type)
    indent =5*level
    result << [(get_spaces(indent)+category.name).html_safe, category.id]
    if selected_category_type.blank? || selected_category_type.id.blank?
      children = Category.find_all_by_parent_id(category.id)
    else
      children = Category.find(:all, :conditions => ['parent_id = ? and id != ?', category.id, selected_category_type.id])
    end
    if !children.blank? && children.size>0
      children.each_with_index do |child_category_type, index|
        get_indented_category_titles(child_category_type, level+1, result, selected_category_type)
      end
    end
  end

  def self.get_spaces(limit)
    result =''
    if limit>0
      for i in 0..limit
        result +='&nbsp;'
      end
    end
    result
  end

  def self.get_categories_by_parent(parent_id, categories)
    if !parent_id.blank?
      Category.active.find_all_by_parent_id(parent_id).each do |category|
        categories<<category
        get_categories_by_parent(category.id, categories)
      end
    end
  end

  def self.admin_get_categories_by_parent(parent_id, categories)
    if !parent_id.blank?
      Category.find_all_by_parent_id(parent_id).each do |category|
        categories<<category
        get_categories_by_parent(category.id, categories)
      end
    end
  end

end
