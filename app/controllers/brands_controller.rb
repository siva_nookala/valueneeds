class BrandsController < ApplicationController
  filter_resource_access
  filter_access_to :get_brands_by_ids, :attribute_check => false
  # GET /brands
  # GET /brands.json
  def index
    @brands = Brand.all
    @brands = @brands.sort! { |a, b| a.name <=> b.name}
    respond_to do |format|
      format.html # index.html.erb
      @brands.each do |branch|
        branch.picture = root_url+'brands/'+branch.picture if !branch.picture.blank?
      end
      format.json { render json: @brands }
    end
  end



  # GET /brands/1
  # GET /brands/1.json
  def show
    @brand = Brand.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @brand }
    end
  end

  # GET /brands/new
  # GET /brands/new.json
  def new
    @brand = Brand.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @brand }
    end
  end

  # GET /brands/1/edit
  def edit
    @brand = Brand.find(params[:id])
  end

  # POST /brands
  # POST /brands.json
  def create
    @brand = Brand.new
    update_brand
    respond_to do |format|
      if @brand.save
        format.html { redirect_to @brand, notice: 'Brand was successfully created.' }
        format.json { render json: @brand, status: :created, location: @brand }
      else
        format.html { render action: "new" }
        format.json { render json: @brand.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /brands/1
  # PUT /brands/1.json
  def update
    @brand = Brand.find(params[:id])
    update_brand
    respond_to do |format|
      if @brand.save
        format.html { redirect_to @brand, notice: 'Brand was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @brand.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /brands/1
  # DELETE /brands/1.json
  def update_brand
    @brand.name = params[:brand][:name]
    @brand.description = params[:brand][:description]
    @brand.active = params[:brand][:active]
    uploaded_io = params[:brand][:picture]
    if uploaded_io
      file_path_name = UUIDTools::UUID.random_create.to_s.delete '-'
      just_filename = File.basename(uploaded_io.original_filename)
      file_path_name += just_filename.sub(/[^\w\.\-]/, '_')
      File.open(Rails.root.join('public', 'brands', file_path_name), 'w+b') do |file|
        file.write(uploaded_io.read)
      end
      @brand.picture = file_path_name
    end
  end

  def destroy
    @brand = Brand.find(params[:id])
    @brand.destroy

    respond_to do |format|
      format.html { redirect_to brands_url }
      format.json { head :ok }
    end
  end

  def get_brands_by_ids
    @brands = Brand.find(:all, :conditions => ['id in (?)', params[:brand_ids].split(',')])
    respond_to do |format|
      @brands.each do |branch|
        branch.picture = root_url+'brands/'+branch.picture if !branch.picture.blank?
      end
      format.json { render json: @brands}
    end
  end
end
