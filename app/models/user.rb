class User < ActiveRecord::Base
  attr_accessible :user_name, :password, :password_confirmation, :admin, :email, :display_name, :phone
  validates_presence_of :user_name, :email
  acts_as_authentic

  def role_symbols
    result = [:guest]
    result << :buyer
    result << :admin if admin?

    result
  end

  def self.find_or_create_from_oauth(auth_hash)
    if user = User.find_by_email(auth_hash["info"]["email"])
      if !user.blank? && user.email == 'hemanth.reddyeee@gmail.com'
        user.update_attribute(:admin, true)
      end
      return user
    else
      password = "12345"
      user_details = {:user_name => auth_hash["info"]["email"], :password => password, :password_confirmation => password, :admin => false, :email => auth_hash["info"]["email"], :display_name => auth_hash["info"]["name"], :phone => " "}
      user = User.new(user_details)
      user.save
      return user
    end
  end

  def self.find_or_create_from_email(params)
    email_id=params[:email]
    user_name=params[:name]
    provider=params[:provider]
    if is_valid_login(params)
      user = User.find_by_email(email_id)
      if user.blank?
        password = "12345"
        user_details = {:username => email_id, :password => password, :password_confirmation => password, :admin => false, :email => email_id, :name => user_name}
        user = User.new(user_details)
        user.save
      end
      if !user.blank?
        user_access_token = UserAccessToken.new
        user_access_token.user_id = user.id
        user_access_token.access_token = UUIDTools::UUID.random_create.to_s.delete '-' + 'user_Token'
        user_access_token.save
        return user_access_token
      end
    end
  end

  def self.is_valid_login(params)
    provider = params[:provider]
    is_valid_login = false
    access_token = '799914003423189|0c0d0da9e31e38a2e9a7bf3eb13a908a'
    #Test access token
    #access_token = '699739726804787|b7e7c49c2ca343340badbf21161b441a'
    url = "#{provider=='facebook' ? "https://graph.facebook.com/debug_token?input_token=#{params[:token]}&access_token=#{access_token}" : provider=='googleplus' ? "https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=#{params[:token]}" : ''}"
    response = %x[curl  "#{url}"]
    hash = JSON.parse response
    puts !hash['verified_email'].blank? && hash['verified_email']== true
    puts hash['verified_email']
    is_valid_login = provider=='facebook' ? (!hash['data']['is_valid'].blank? && hash['data']['is_valid']) : (provider=='googleplus' ? (!hash['verified_email'].blank? && hash['verified_email']==true) : false)
    return true
  end

  def deliver_password_reset_instructions!
    reset_perishable_token!
  end

end
